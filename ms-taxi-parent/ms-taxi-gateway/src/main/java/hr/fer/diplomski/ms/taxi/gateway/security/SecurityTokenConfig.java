package hr.fer.diplomski.ms.taxi.gateway.security;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter{
	
	private final Logger LOGGER = LoggerFactory.getLogger(SecurityTokenConfig.class);

	@Autowired
	private JwtConfig jwtConfig;
	
	@Override
  	protected void configure(HttpSecurity http) throws Exception {
		 http
			.csrf().disable()
			    // make sure we use stateless session; session won't be used to store user's state.
		 	    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) 	
			.and()
			    // handle an authorized attempts 
			    .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)) 	
			.and()
			   // Add a filter to validate the tokens with every request
			   .addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
			// authorization requests config
			.authorizeRequests()
			
			   // allow all who are accessing "auth" service
			   .antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()  
			  // .antMatchers(HttpMethod.GET, jwtConfig.getUri()).permitAll()  
			   // Any other request must be authenticated
			   .anyRequest().authenticated(); 
	}
	
	@Bean
  	public JwtConfig jwtConfig() {
		
		LOGGER.info("Instatiating JWT config");
		JwtConfig jwtConfig = new JwtConfig();
		LOGGER.info("Instatiated JWT config: {}", jwtConfig);
    	return jwtConfig;
  	}

}
