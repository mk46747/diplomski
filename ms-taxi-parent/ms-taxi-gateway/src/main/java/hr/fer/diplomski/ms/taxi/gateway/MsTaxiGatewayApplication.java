package hr.fer.diplomski.ms.taxi.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class MsTaxiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsTaxiGatewayApplication.class, args);
	}
}
