package hr.fer.diplomski.ms.taxi.trip.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.LocalDateTimeSerializer;

@Entity
@Table(name = "tx_ms_tp_trip")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Trip implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_ms_tp_trip")
	@SequenceGenerator(name = "seq_tx_ms_tp_trip", sequenceName = "seq_tx_ms_tp_trip", allocationSize = 1)
	@Column(name = "trip_id", nullable = false)
	private Long tripId;
	
	@Column(name = "trip_user_id", nullable = false)
	private Long userId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "taxi_id")
	private Taxi taxi;
	
	@ManyToOne( fetch = FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "trip_address_start_id")
	private Address start;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "trip_address_dest_id")
	private Address destination;
	
	@Column(name = "trip_distance", nullable = false)
	private long distance;
	
	@JsonDeserialize(using= LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "trip_start_time")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime startTime;
	
	@JsonDeserialize(using= LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "trip_end_time")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime endTime;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "fpl_id")
	private FarePriceList farePriceList;
	
	@Enumerated(EnumType.STRING)
	private TripStatus tripStatus;
	
	@Column(name = "trip_base_fare", nullable = false)
	private BigDecimal baseFare;
	
	@Column(name = "trip_time_fare", nullable = false)
	private BigDecimal timeFare;
	
	@Column(name = "trip_distance_fare", nullable = false)
	private BigDecimal distanceFare;
	

	public Long getTripId() {
		return tripId;
	}

	public void setTripId(Long id) {
		this.tripId = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Taxi getTaxi() {
		return taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	public Address getStart() {
		return start;
	}

	public void setStart(Address start) {
		this.start = start;
	}

	public Address getDestination() {
		return destination;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public FarePriceList getFarePriceList() {
		return farePriceList;
	}

	public void setFarePriceList(FarePriceList farePriceList) {
		this.farePriceList = farePriceList;
	}

	public TripStatus getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(TripStatus tripStatus) {
		this.tripStatus = tripStatus;
	}

	public BigDecimal getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(BigDecimal baseFare) {
		this.baseFare = baseFare;
	}

	public BigDecimal getTimeFare() {
		return timeFare;
	}

	public void setTimeFare(BigDecimal timeFare) {
		this.timeFare = timeFare;
	}

	public BigDecimal getDistanceFare() {
		return distanceFare;
	}

	public void setDistanceFare(BigDecimal distanceFare) {
		this.distanceFare = distanceFare;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(tripId);
		sb.append(", userId: ");
		sb.append(userId);
		sb.append(", taxi: ");
		sb.append(taxi);		
		sb.append(", start: ");
		sb.append(start);
		sb.append(", dest: ");
		sb.append(destination);
		sb.append(", distance: ");
		sb.append(distance);
		sb.append(", startTime: ");
		sb.append(startTime);		
		sb.append(", endTime: ");
		sb.append(endTime);	
		sb.append(", fpl: ");
		sb.append(farePriceList);	
		sb.append(", status: ");
		sb.append(tripStatus);	
		sb.append("]");
		return sb.toString();
	}

}
