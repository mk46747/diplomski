package hr.fer.diplomski.ms.taxi.trip.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import hr.fer.diplomski.ms.taxi.trip.model.Trip;


public class Sender {

	private final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

	@Autowired
	private KafkaTemplate<String, Trip> kafkaTemplate;

	
	public void send(String topic, Trip payload){
		LOGGER.info("Sending payload: {} to topic: {}", payload, topic);
		
		kafkaTemplate.send(topic, payload);
	}

}
