package hr.fer.diplomski.ms.taxi.trip.service;

import hr.fer.diplomski.ms.taxi.trip.model.Trip;

public interface TripKafkaService {

	void sendTripToKafka(Trip trip);
}
