package hr.fer.diplomski.ms.taxi.trip.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tx_ms_tp_country")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Country implements Serializable {

	private static final long serialVersionUID = -2662431433744649255L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_country")
	@SequenceGenerator(name = "seq_tx_country", sequenceName = "seq_tx_country", allocationSize = 1)
	@Column(name = "country_id", nullable = false)
	private Long id;

	@Column(name = "country_name", nullable = false)
	private String name;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(name);
		sb.append("]");
		return sb.toString();
	}
	
	
}
