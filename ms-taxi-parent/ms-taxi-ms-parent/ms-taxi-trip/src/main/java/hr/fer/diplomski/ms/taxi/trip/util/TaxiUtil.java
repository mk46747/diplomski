package hr.fer.diplomski.ms.taxi.trip.util;

import hr.fer.diplomski.ms.taxi.trip.model.Taxi;
import hr.fer.diplomski.ms.taxi.trip.model.TaxiStatus;

public class TaxiUtil {

	public static final String TAXI_REGISTRATION_NUMBER_PREFIX = "taxi_reg_num_";
	public static final String DRIVER_VEHICLE_MODEL_PREFIX = "taxi_vehicle_model_";

	public static Taxi generateTaxi(int index, long driverId){
		Taxi taxi = new Taxi();
		taxi.setDriverId(driverId);
		taxi.setRegistrationNumber(TAXI_REGISTRATION_NUMBER_PREFIX + index);
		taxi.setVehicleModel(DRIVER_VEHICLE_MODEL_PREFIX + index);
		taxi.setTaxiStatus(TaxiStatus.FREE);
		return taxi;
	}
}
