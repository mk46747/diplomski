package hr.fer.diplomski.ms.taxi.trip.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.trip.model.Trip;


@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {
	
	List<Trip> findByUserId(Long id);

}
