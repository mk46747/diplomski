package hr.fer.diplomski.ms.taxi.trip.service.impl;


import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.trip.model.Address;
import hr.fer.diplomski.ms.taxi.trip.model.Taxi;
import hr.fer.diplomski.ms.taxi.trip.model.TaxiStatus;
import hr.fer.diplomski.ms.taxi.trip.repository.TaxiRepository;
import hr.fer.diplomski.ms.taxi.trip.service.TaxiService;


@Service
public class TaxiServiceImpl implements TaxiService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(TaxiServiceImpl.class);

	
	private final TaxiRepository taxiRepository;
	
	
	@Autowired
	public TaxiServiceImpl(final TaxiRepository taxiRepository) {
		this.taxiRepository = taxiRepository;
	}

	@Override
	public Taxi findTaxi(Address start) {
		Taxi taxi = null;
		try{
			taxi =  taxiRepository.findFirstByTaxiStatus(TaxiStatus.FREE);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity Taxi not found exception for status:{}", TaxiStatus.FREE);
			//TODO
			return null;
		}
		 
		LOGGER.info("Fetched Taxi by address: {} - {}",start, taxi);
		if(taxi == null){
			return null;
		}
		return taxi;
	}

	@Override
	public Taxi updateTaxi(Taxi taxi) {
		Taxi updatedTaxi = taxiRepository.save(taxi);
		LOGGER.info("Saved Taxi - {}", taxi);
		if(taxi == null){
			return null;
		}
		return updatedTaxi;	
	}

	

}
