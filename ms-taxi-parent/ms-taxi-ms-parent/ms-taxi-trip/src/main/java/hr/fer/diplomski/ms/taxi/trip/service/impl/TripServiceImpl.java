package hr.fer.diplomski.ms.taxi.trip.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.trip.model.Address;
import hr.fer.diplomski.ms.taxi.trip.model.FarePriceList;
import hr.fer.diplomski.ms.taxi.trip.model.Taxi;
import hr.fer.diplomski.ms.taxi.trip.model.TaxiStatus;
import hr.fer.diplomski.ms.taxi.trip.model.Trip;
import hr.fer.diplomski.ms.taxi.trip.model.TripStatus;
import hr.fer.diplomski.ms.taxi.trip.repository.TripRepository;
import hr.fer.diplomski.ms.taxi.trip.service.FarePriceListService;
import hr.fer.diplomski.ms.taxi.trip.service.TaxiService;
import hr.fer.diplomski.ms.taxi.trip.service.TripKafkaService;
import hr.fer.diplomski.ms.taxi.trip.service.TripService;

@Service
public class TripServiceImpl implements TripService{

	private final Logger LOGGER = LoggerFactory.getLogger(TripServiceImpl.class);
	
	private final TripRepository tripRepository;
	
	private final TaxiService taxiService;
	
	private final FarePriceListService farePriceListService;
	
	private final TripKafkaService tripKafkaService;
	
	@Autowired
	public TripServiceImpl(final TripRepository tripRepository, final TaxiService taxiService, 
			final FarePriceListService farePriceListService, final TripKafkaService tripKafkaService) {
		this.tripRepository = tripRepository;
		this.taxiService = taxiService;
		this.farePriceListService = farePriceListService;
		this.tripKafkaService = tripKafkaService;
	}
	


	@Override
	public Trip createTrip(Long userId, Address start, Address destination) {
		LOGGER.info("Creating trip - userId: {}, start:{}, destination: {}", userId, start, destination);
		
		FarePriceList fpl = farePriceListService.getFarePriceList();
		LOGGER.info("Fetched farePriceList: {} ", fpl);
		
		Taxi taxi = taxiService.findTaxi(start);
		LOGGER.info("Fetched taxi: {} for location: {}", taxi, start);
		
		LOGGER.info("Updating taxi - start trip - taxi: {}, start location: {}", taxi, start);
		taxi.setLocation(start);
		taxi.setTaxiStatus(TaxiStatus.BUSY);
		taxiService.updateTaxi(taxi);
		
		Trip trip = new Trip();
		trip.setUserId(userId);
		trip.setTaxi(taxi);
		trip.setFarePriceList(fpl);
		
		trip.setStart(start);
		trip.setDestination(destination);
		
		//mock racunanje...
		trip.setBaseFare(fpl.getBaseFarePrice());
		trip.setTimeFare(fpl.getTimeFarePricePerMin().multiply(new BigDecimal(5.2)));
		trip.setDistanceFare(fpl.getTimeFarePricePerMin().multiply(new BigDecimal(9.5)));
			
		trip.setDistance(25);
		trip.setTripStatus(TripStatus.PENDING);
		
		return saveTrip(trip);
	}

	@Override
	public Trip startTrip(Long tripId) {
		Trip trip = fetchTrip(tripId);
		if(trip == null){
			LOGGER.error("Could not find trip with id: {}", tripId);
			return null;
		}

		trip.setStartTime(LocalDateTime.now());
		trip.setTripStatus(TripStatus.STARTED);
		
		return saveTrip(trip);
		
	}

	@Override
	public Trip finishTrip(Long tripId) {
		Trip trip = fetchTrip(tripId);
		if(trip == null){
			LOGGER.error("Could not find trip with id: {}", tripId);
			return null;
		}

		trip.setEndTime(LocalDateTime.now());
		trip.setTripStatus(TripStatus.FINISHED);
		
		Taxi taxi = trip.getTaxi();	
		taxi.setLocation(trip.getDestination());
		taxi.setTaxiStatus(TaxiStatus.FREE);
		LOGGER.info("Updating taxi - end trip - taxi: {}, end location{}: {}", taxi, trip.getDestination());

		taxiService.updateTaxi(taxi);
				
		Trip savedTrip = saveTrip(trip);
		tripKafkaService.sendTripToKafka(savedTrip);
		return savedTrip;
	}
	

	@Override
	public List<Trip> getTripsForUser(Long userId) {
		LOGGER.info("Fetching Trip list for user with id:{}", userId);
		
		List<Trip> trips = null;
		try{
			trips = tripRepository.findByUserId(userId);
		}catch(EntityNotFoundException e){
			LOGGER.info("EntityNotFoundException while finding trips for user with id:{}", userId);
			//TODO exception...
			trips = new ArrayList<Trip>();
		}
		
		LOGGER.info("Fetched Trip list for user with id:{} - {}",userId, trips);
		if(trips != null){
			LOGGER.info("Fetched {} Trip objects for user with id:{}", trips.size(), userId);
		}

		return trips;
	}
	
	@Override
	public Trip fetchTrip(Long tripId) {
		Trip trip = null;
		try{
			trip = tripRepository.getOne(tripId);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity Trip not foundexception for id:{}", tripId);
			//TODO exception...
			return null;
		}		
		LOGGER.info("Fetched Trip for id:{} - {}", tripId, trip);
		
		return trip;
	}
	
	private Trip saveTrip(Trip trip){
		Trip savedTrip = tripRepository.save(trip);		
		LOGGER.info("Saved Trip: {}", savedTrip);
		if(trip == null){
			return null;
		}
		return trip;
	}
	
	
	
}
