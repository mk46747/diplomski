package hr.fer.diplomski.ms.taxi.trip.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.trip.kafka.Sender;
import hr.fer.diplomski.ms.taxi.trip.model.Trip;
import hr.fer.diplomski.ms.taxi.trip.service.TripKafkaService;

@Service
public class TripKafkaServiceImpl implements TripKafkaService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(TripKafkaServiceImpl.class);
	
    @Value("${spring.kafka.topic.trip-topic}")
	private String topic;
    
    private Sender sender;
    
    @Autowired
    public TripKafkaServiceImpl(Sender sender){
    	this.sender = sender;
    }

	@Override
	public void sendTripToKafka(Trip trip) {
		LOGGER.info("Sending Trip: {} to kafka", trip);
		sender.send(topic, trip);
	}

}
