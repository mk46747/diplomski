package hr.fer.diplomski.ms.taxi.trip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MsTaxiTripApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsTaxiTripApplication.class, args);
	}
}
