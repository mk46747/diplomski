package hr.fer.diplomski.ms.taxi.trip.service;

import hr.fer.diplomski.ms.taxi.trip.model.FarePriceList;

public interface FarePriceListService {
	
	FarePriceList getFarePriceList();

}
