package hr.fer.diplomski.ms.taxi.trip.service;

import hr.fer.diplomski.ms.taxi.trip.model.Address;
import hr.fer.diplomski.ms.taxi.trip.model.Taxi;

public interface TaxiService {

	Taxi findTaxi(Address start);

	Taxi updateTaxi(Taxi taxi);

}