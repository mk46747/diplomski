package hr.fer.diplomski.ms.taxi.trip;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import hr.fer.diplomski.ms.taxi.trip.model.FarePriceList;
import hr.fer.diplomski.ms.taxi.trip.model.Taxi;
import hr.fer.diplomski.ms.taxi.trip.repository.FarePriceListRepository;
import hr.fer.diplomski.ms.taxi.trip.repository.TaxiRepository;
import hr.fer.diplomski.ms.taxi.trip.util.FplUtil;
import hr.fer.diplomski.ms.taxi.trip.util.TaxiUtil;





@Component
public class Initializer implements CommandLineRunner {
	
	private final Logger LOGGER = LoggerFactory.getLogger(Initializer.class);
	
	private static final int TAXI_NUMBER = 12000;

	private final TaxiRepository taxiRepository;
	
	private final FarePriceListRepository farePriceListRepository;
	
	@Autowired
	public Initializer(final TaxiRepository taxiRepository, final FarePriceListRepository farePriceListRepository){
		this.taxiRepository = taxiRepository;
		this.farePriceListRepository = farePriceListRepository;
	}

	@Override
	public void run(String... args) throws Exception {
				
		LOGGER.info("Generating {} Taxi objects...", TAXI_NUMBER);
		List<Taxi> taxiList = new ArrayList<Taxi>();
		for(int i = 0; i < TAXI_NUMBER; i++){
			Taxi taxi = TaxiUtil.generateTaxi(i, i + 1);
			taxiList.add(taxi);
		}
		LOGGER.info("Saving Taxi objects.");
		taxiRepository.saveAll(taxiList);		
		LOGGER.info("Taxi objects saved.");

		LOGGER.info("Generating FarePriceList object...");
		FarePriceList fpl = FplUtil.generateFarePriceList();
		farePriceListRepository.save(fpl);
		LOGGER.info("FarePriceList object saved.");
	}
	
	
	
	

}
