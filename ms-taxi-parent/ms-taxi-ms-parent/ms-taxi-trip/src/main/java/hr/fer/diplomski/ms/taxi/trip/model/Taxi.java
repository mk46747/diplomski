package hr.fer.diplomski.ms.taxi.trip.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tx_ms_tp_taxi")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Taxi implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_ms_tx_taxi")
	@SequenceGenerator(name = "seq_tx_ms_tx_taxi", sequenceName = "seq_tx_ms_tx_taxi", allocationSize = 1)
	@Column(name = "taxi_id", nullable = false)
	private Long id;
	
	@Column(name = "driver_id", nullable = false)
	private Long driverId;

	@Column(name = "taxi_reg_number", nullable = false)
	private String registrationNumber;
	
	@Column(name = "taxi_vehicle_model", nullable = false)
	private String vehicleModel;
	
	@OneToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "taxi_address_id")
	private Address location;
	
	@Enumerated(EnumType.STRING)
	private TaxiStatus taxiStatus;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}


	public TaxiStatus getTaxiStatus() {
		return taxiStatus;
	}

	public void setTaxiStatus(TaxiStatus taxiStatus) {
		this.taxiStatus = taxiStatus;
	}

	public Address getLocation() {
		return location;
	}

	public void setLocation(Address location) {
		this.location = location;
	} 
	
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", driverId: ");
		sb.append(driverId);
		sb.append(", ");
		sb.append(registrationNumber);
		sb.append(", ");
		sb.append(vehicleModel);
		sb.append(", ");
		sb.append(location);	
		sb.append(", ");
		sb.append(taxiStatus);
		sb.append("]");
		return sb.toString();
	}
	
	
	
}
