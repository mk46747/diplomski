package hr.fer.diplomski.ms.taxi.trip.model;

import java.util.HashMap;
import java.util.Map;

public enum TripStatus {
	
	PENDING("pending"),
	STARTED("started"),
	FINISHED("finished");
	
	private static final Map<String, TripStatus> LOOKUP = new HashMap<String, TripStatus>();
	
	static {
		for (final TripStatus tripStatus : TripStatus.values()) {
			LOOKUP.put(tripStatus.getCode(), tripStatus);
		}
	}	
	
	public static TripStatus getTripStatus(final String p_code) {
		return LOOKUP.get(p_code);
	}

	private String m_code;

	private TripStatus(final String p_code) {
		m_code = p_code;
	}

	public String getCode() {
		return m_code;
	}

	public void setCode(final String p_code) {
		m_code = p_code;
	}

}
