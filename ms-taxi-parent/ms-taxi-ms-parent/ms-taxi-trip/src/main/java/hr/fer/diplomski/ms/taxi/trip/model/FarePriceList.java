package hr.fer.diplomski.ms.taxi.trip.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "tx_ms_tp_fare_price_list")
public class FarePriceList implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_fare_price_list")
	@SequenceGenerator(name = "seq_tx_fare_price_list", sequenceName = "seq_tx_fare_price_list", allocationSize = 1)
	@Column(name = "fpl_id", nullable = false)
	private Long id;
	
	@Column(name = "fpl_type")
	@Enumerated(EnumType.STRING)
	private FarePriceListType farePriceListType;
	
	@Column(name = "fpl_base_fare_price", nullable = false)
	private BigDecimal baseFarePrice;
	
	@Column(name = "fpl_time_fare_price", nullable = false)
	private BigDecimal timeFarePricePerMin;
	
	@Column(name = "fpl_distance_fare_price", nullable = false)
	private BigDecimal distanceFarePricePerKm;
	
	@Column(name = "fpl_active")
	private Boolean active;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FarePriceListType getFarePriceListType() {
		return farePriceListType;
	}

	public void setFarePriceListType(FarePriceListType farePriceListType) {
		this.farePriceListType = farePriceListType;
	}

	public BigDecimal getBaseFarePrice() {
		return baseFarePrice;
	}

	public void setBaseFarePrice(BigDecimal baseFarePrice) {
		this.baseFarePrice = baseFarePrice;
	}

	public BigDecimal getTimeFarePricePerMin() {
		return timeFarePricePerMin;
	}

	public void setTimeFarePricePerMin(BigDecimal timeFarePricePerMin) {
		this.timeFarePricePerMin = timeFarePricePerMin;
	}

	public BigDecimal getDistanceFarePricePerKm() {
		return distanceFarePricePerKm;
	}

	public void setDistanceFarePricePerKm(BigDecimal distanceFarePricePerKm) {
		this.distanceFarePricePerKm = distanceFarePricePerKm;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(baseFarePrice);
		sb.append(", ");
		sb.append(timeFarePricePerMin);
		sb.append(", ");		
		sb.append(distanceFarePricePerKm);
		sb.append(", ");		
		sb.append(active);
		sb.append("]");
		return sb.toString();

	}

	
}
