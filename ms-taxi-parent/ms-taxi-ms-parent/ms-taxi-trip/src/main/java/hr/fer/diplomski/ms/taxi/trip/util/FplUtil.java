package hr.fer.diplomski.ms.taxi.trip.util;

import java.math.BigDecimal;

import hr.fer.diplomski.ms.taxi.trip.model.FarePriceList;
import hr.fer.diplomski.ms.taxi.trip.model.FarePriceListType;




public class FplUtil {

	public static FarePriceList generateFarePriceList(){
		FarePriceList fpl = new FarePriceList();
		
		fpl.setActive(Boolean.TRUE);
		fpl.setBaseFarePrice(new BigDecimal(15));
		fpl.setDistanceFarePricePerKm(new BigDecimal(5.5));
		fpl.setTimeFarePricePerMin(new BigDecimal(2.75));
		fpl.setFarePriceListType(FarePriceListType.NORMAL);
		
		return fpl;
	}
}
