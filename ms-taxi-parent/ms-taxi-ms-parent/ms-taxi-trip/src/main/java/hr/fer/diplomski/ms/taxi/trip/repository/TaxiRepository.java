package hr.fer.diplomski.ms.taxi.trip.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.trip.model.Taxi;
import hr.fer.diplomski.ms.taxi.trip.model.TaxiStatus;


@Repository
public interface TaxiRepository extends JpaRepository<Taxi, Long>{

	Taxi findFirstByTaxiStatus(TaxiStatus taxiStatus);
}
