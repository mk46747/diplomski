//package hr.fer.diplomski.ms.taxi.trip.util;
//
//import java.math.BigDecimal;
//
//import hr.fer.diplomski.ms.taxi.trip.model.Address;
//import hr.fer.diplomski.ms.taxi.trip.model.City;
//import hr.fer.diplomski.ms.taxi.trip.model.Country;
//import hr.fer.diplomski.ms.taxi.trip.model.FarePriceList;
//import hr.fer.diplomski.ms.taxi.trip.model.FarePriceListType;
//import hr.fer.diplomski.ms.taxi.trip.model.Taxi;
//import hr.fer.diplomski.ms.taxi.trip.model.TaxiStatus;
//
//public class TripUtil {
//
//	public static Address createTestAddress(){
//		Address address = new Address();
//		Country country = new Country();
//		country.setName("Hrvatska");
//		City city = new City();
//		city.setName("Zagreb");
//		city.setCountry(country);
//		address.setCity(city);
//		address.setHouseNumber("3");
//		address.setLatitude(Long.valueOf(123));
//		address.setLongitude(Long.valueOf(321));
//		address.setStreet("Unska");
//		
//		return address;
//	}
//	
//	public static Taxi createTestTaxi(Long driverId){
//		Taxi taxi = new Taxi();
//		taxi.setDriverId(driverId);
//		taxi.setLocation(createTestAddress());
//		taxi.setRegistrationNumber("ZG-500");
//		taxi.setTaxiStatus(TaxiStatus.FREE);
//		taxi.setVehicleModel("audi 1000");
//		return taxi;
//	}
//	
//	public static FarePriceList createTestFpl(){
//		FarePriceList fpl = new FarePriceList();
//		
//		fpl.setActive(Boolean.TRUE);
//		fpl.setBaseFarePrice(new BigDecimal(20));
//		fpl.setDistanceFarePricePerKm(new BigDecimal(10));
//		fpl.setTimeFarePricePerMin(new BigDecimal(30));
//		fpl.setFarePriceListType(FarePriceListType.NORMAL);
//		
//		return fpl;
//		
//	}
//}
