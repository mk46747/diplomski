package hr.fer.diplomski.ms.taxi.trip.service;

import java.util.List;

import hr.fer.diplomski.ms.taxi.trip.model.Address;
import hr.fer.diplomski.ms.taxi.trip.model.Trip;

public interface TripService {

	Trip createTrip(Long userId, Address start, Address destination);
	
	Trip startTrip(Long tripId);
	
	Trip finishTrip(Long tripId);

	List<Trip> getTripsForUser(Long userId);
	
	Trip fetchTrip(Long tripId);
}
