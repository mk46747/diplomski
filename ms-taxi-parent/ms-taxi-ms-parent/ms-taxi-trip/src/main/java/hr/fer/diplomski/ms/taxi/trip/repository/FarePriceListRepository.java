package hr.fer.diplomski.ms.taxi.trip.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.trip.model.FarePriceList;


@Repository
public interface FarePriceListRepository extends JpaRepository<FarePriceList, Long> {
	
	FarePriceList findByActive(Boolean active);
}
