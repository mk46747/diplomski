package hr.fer.diplomski.ms.taxi.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.billing.repository.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	
	User findByPersonMsId(Long personMsId);
			
}