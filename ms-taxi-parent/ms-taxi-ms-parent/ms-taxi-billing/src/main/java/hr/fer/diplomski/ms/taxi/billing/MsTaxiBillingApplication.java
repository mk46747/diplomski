package hr.fer.diplomski.ms.taxi.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MsTaxiBillingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsTaxiBillingApplication.class, args);
	}
}
