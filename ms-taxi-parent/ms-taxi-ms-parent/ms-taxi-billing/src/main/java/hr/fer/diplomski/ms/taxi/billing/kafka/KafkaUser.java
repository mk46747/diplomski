package hr.fer.diplomski.ms.taxi.billing.kafka;

import java.io.Serializable;

public class KafkaUser implements Serializable{

	private static final long serialVersionUID = 3546553439508618935L;

	private Long userId;
	
	private String userFirstName;
	
	private String userLastName;
	
	private String userCreditCardNumber;
	
	private String email;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserCreditCardNumber() {
		return userCreditCardNumber;
	}

	public void setUserCreditCardNumber(String creditCardNumber) {
		this.userCreditCardNumber = creditCardNumber;
	}
	
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[userId:");
		sb.append(userId);
		sb.append(", userFirstName: ");
		sb.append(userFirstName);
		sb.append(", userLastName: ");		
		sb.append(userLastName);
		sb.append(", userCreditCardNumber: ");		
		sb.append(userCreditCardNumber);
		sb.append(", email: ");		
		sb.append(email);
		sb.append("]");
		return sb.toString();

	}
	
}
