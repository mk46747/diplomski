package hr.fer.diplomski.ms.taxi.billing.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.LocalDateTimeSerializer;


public class Trip implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long tripId;
	
	private Long userId;

	private Taxi taxi;

	private Address start;

	private Address destination;
	
	private long distance;
	@JsonDeserialize(using= LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime startTime;
	@JsonDeserialize(using= LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime endTime;

	private FarePriceList farePriceList;
	
	private TripStatus tripStatus;
	
	private BigDecimal baseFare;
	
	private BigDecimal timeFare;
	
	private BigDecimal distanceFare;
	

	public Long getTripId() {
		return tripId;
	}

	public void setTripId(Long id) {
		this.tripId = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Taxi getTaxi() {
		return taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	public Address getStart() {
		return start;
	}

	public void setStart(Address start) {
		this.start = start;
	}

	public Address getDestination() {
		return destination;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public FarePriceList getFarePriceList() {
		return farePriceList;
	}

	public void setFarePriceList(FarePriceList farePriceList) {
		this.farePriceList = farePriceList;
	}

	public TripStatus getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(TripStatus tripStatus) {
		this.tripStatus = tripStatus;
	}

	public BigDecimal getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(BigDecimal baseFare) {
		this.baseFare = baseFare;
	}

	public BigDecimal getTimeFare() {
		return timeFare;
	}

	public void setTimeFare(BigDecimal timeFare) {
		this.timeFare = timeFare;
	}

	public BigDecimal getDistanceFare() {
		return distanceFare;
	}

	public void setDistanceFare(BigDecimal distanceFare) {
		this.distanceFare = distanceFare;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(tripId);
		sb.append(", userId: ");
		sb.append(userId);
		sb.append(", taxi: ");
		sb.append(taxi);		
		sb.append(", start: ");
		sb.append(start);
		sb.append(", dest: ");
		sb.append(destination);
		sb.append(", distance: ");
		sb.append(distance);
		sb.append(", startTime: ");
		sb.append(startTime);		
		sb.append(", endTime: ");
		sb.append(endTime);	
		sb.append(", fpl: ");
		sb.append(farePriceList);	
		sb.append(", status: ");
		sb.append(tripStatus);	
		sb.append("]");
		return sb.toString();
	}

}
