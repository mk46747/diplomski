package hr.fer.diplomski.ms.taxi.billing.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.billing.model.Address;
import hr.fer.diplomski.ms.taxi.billing.model.City;
import hr.fer.diplomski.ms.taxi.billing.model.Country;
import hr.fer.diplomski.ms.taxi.billing.model.Taxi;
import hr.fer.diplomski.ms.taxi.billing.model.Trip;
import hr.fer.diplomski.ms.taxi.billing.repository.model.Driver;
import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;
import hr.fer.diplomski.ms.taxi.billing.repository.model.User;
import hr.fer.diplomski.ms.taxi.billing.service.BillingService;
import hr.fer.diplomski.ms.taxi.billing.service.DriverService;
import hr.fer.diplomski.ms.taxi.billing.service.ReceiptService;
import hr.fer.diplomski.ms.taxi.billing.service.UserService;



@Service
public class BillingServiceImpl implements BillingService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(BillingServiceImpl.class);
	
	private final UserService userService;
	
	private final DriverService driverService;
	
	private final ReceiptService receiptService;
	
	
	@Autowired
	public BillingServiceImpl(final UserService userService, final DriverService driverService, final ReceiptService receiptService){
		this.userService = userService;
		this.driverService = driverService;
		this.receiptService = receiptService;
	}
	

	@Override
	public void processTrip(Trip trip) {
		Receipt receipt = composeReceipt(trip);
		if(receipt == null){
			LOGGER.error("Composed receipt is null!");
			return;
		}
		LOGGER.info("Composed receipt: {}", receipt);
		boolean chargingSuccess = chargeTrip(receipt);
		if(chargingSuccess == false){
			LOGGER.error("Charging failed...");
		}
		

		LOGGER.info("Saving receipt: {}", receipt);
		Receipt savedReceipt = receiptService.saveReceipt(receipt);
		if(savedReceipt == null){
			LOGGER.error("Saved receipt is null!");
			return;
		}
		
		boolean emailSuccess = sendReceiptToEmail(receipt);
		if(emailSuccess == false){
			LOGGER.error("Sending to email failed...");
		}
	}

	private boolean sendReceiptToEmail(Receipt receipt) {
		LOGGER.info("Invoked send receipt to email");
		byte[] receiptPdf = receiptService.generatePdfReceipt(receipt);
		LOGGER.info("Generated pdf receipt, size: {} bytes", receiptPdf == null ? 0 : receiptPdf.length);
		String email = receipt.getUserEmail();
		LOGGER.info("Sending pdf receipt to: {}", email);
		LOGGER.info("...");
		LOGGER.info("...");
		LOGGER.info("Pdf receipt sent");
		return Boolean.TRUE;

	}


	private boolean chargeTrip(Receipt receipt) {
		LOGGER.info("Charging trip - creditCard: {} , total fare: {}, user first name: {}", receipt.getUserCreditCard(), receipt.getTripTotalFare(), receipt.getUserFirstName());
		LOGGER.info("...");
		LOGGER.info("...");
		LOGGER.info("Charged!");
		return Boolean.TRUE;
	}


	private Receipt composeReceipt(Trip trip){
		if(trip == null){
			LOGGER.error("Compose receipt - trip is null!");
			return null;
		}
		Long userId = trip.getUserId();
		User user = userService.findByPersonMsId(userId);
		if(user == null){
			LOGGER.error("Compose receipt - user is null for userId: {}!", userId);
			return null;			
		}
		
		Taxi taxi = trip.getTaxi();
		if(taxi == null){
			LOGGER.error("Compose receipt - taxi is null! - Trip: {}", trip);
			return null;			
		}
		
		Long driverId = taxi.getDriverId();
		Driver driver = driverService.findByPersonMsId(driverId);
		
		if(driver == null){
			LOGGER.error("Compose receipt - driver is null for driverId: {}!", driver);
			return null;			
		}
		
		Receipt receipt = new Receipt();
		receipt.setTripId(trip.getTripId().toString());
		
		receipt.setUserFirstName(user.getFirstName());
		receipt.setUserLastName(user.getLastName());
		receipt.setUserEmail(user.getEmail());
		receipt.setUserCreditCard(user.getCreditCardNumber());
		
		receipt.setDriverFirstName(driver.getFirstName());
		receipt.setDriverLastName(driver.getLastName());
		receipt.setDriverLicenceNumber(driver.getLicenceNumber());
		
		receipt.setTaxiVehicleModel(taxi.getRegistrationNumber());
		receipt.setTaxiRegistrationNumber(taxi.getRegistrationNumber());
		
		receipt.setTripStartTime(convertLocalDateTimeToString(trip.getStartTime()));
		receipt.setTripEndTime(convertLocalDateTimeToString(trip.getEndTime()));
		
		receipt.setTripStartAddress(convertAddressToString(trip.getStart()));
		receipt.setTripDestinationAddress(convertAddressToString(trip.getDestination()));
		
		receipt.setTripDistance(convertDistanceToString(trip.getDistance()));
		receipt.setTripDuration(convertDurationToString(trip));
		
		receipt.setTripBaseFare(convertFareToString(trip.getBaseFare()));
		receipt.setTripTimeFare(convertFareToString(trip.getTimeFare()));
		receipt.setTripDistanceFare(convertFareToString(trip.getDistanceFare()));
		receipt.setTripTotalFare(calculateTotalFare(trip));
		
		return receipt;
	}
	
	private String calculateTotalFare(Trip trip) {
		BigDecimal totalFare = new BigDecimal(0);
		BigDecimal baseFare = trip.getBaseFare();
		BigDecimal distanceFare = trip.getDistanceFare();
		BigDecimal timeFare = trip.getTimeFare();
		
		totalFare = totalFare.add(baseFare == null ? new BigDecimal(0) : baseFare);
		totalFare = totalFare.add(distanceFare == null ? new BigDecimal(0) : distanceFare);
		totalFare = totalFare.add(timeFare == null ? new BigDecimal(0) : timeFare);
		
		return convertFareToString(totalFare);
	}


	private String convertFareToString(BigDecimal value) {
		DecimalFormat df = new DecimalFormat("#,###.00");
	    return df.format(value) + " kn";
	}


	private String convertDurationToString(Trip trip){
		LocalDateTime start = trip.getStartTime();
		LocalDateTime end = trip.getEndTime();
		if(start == null || end == null){
			return "";
		}
		Period duration = new Period(start, end);
		
		PeriodFormatter hoursMinutes = new PeriodFormatterBuilder()
				.appendHours()
			    .appendSeparator(" hour", " hours")
			    .appendMinutes()
			    .appendSuffix(" minute", " minutes")
			    .appendSeconds()
			    .appendSuffix(" second", " seconds")
			    .toFormatter();
		return hoursMinutes.print(duration);
	}
	
	private String convertDistanceToString(Long distance){
		return distance + " km";
	}
	
	private String convertLocalDateTimeToString(LocalDateTime dateTime){
		return dateTime == null ? "" : dateTime.toString(DateTimeFormat.fullDate());
	}
	
	
	private String convertAddressToString(Address address){
		if(address == null){
			return null;
		}
		String cityName = "";
		String countryName = "";
		
		City city = address.getCity();
		if(city != null){
			cityName = city.getName();
			Country country = city.getCountry();
			if(country != null){
				countryName = country.getName();
			}
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(address.getStreet());
		sb.append(" ");
		sb.append(address.getHouseNumber());
		sb.append(", ");
		sb.append(cityName);
		sb.append(", ");
		sb.append(countryName);
		return sb.toString();

	}
	
	

	
}
