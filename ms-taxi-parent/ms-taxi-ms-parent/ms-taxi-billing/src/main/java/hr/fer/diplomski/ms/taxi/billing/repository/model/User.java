package hr.fer.diplomski.ms.taxi.billing.repository.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;




@Entity
@DiscriminatorValue("person_user")  
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User extends Person{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "user_email")
	private String email;
	
	@Column(name = "user_credit_card_num")
	private String creditCardNumber;
	


	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getCreditCardNumber() {
		return creditCardNumber;
	}



	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}



	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(email);
		sb.append(", ");		
		sb.append(creditCardNumber);
		sb.append("]");
		return sb.toString();

	}
	
}
