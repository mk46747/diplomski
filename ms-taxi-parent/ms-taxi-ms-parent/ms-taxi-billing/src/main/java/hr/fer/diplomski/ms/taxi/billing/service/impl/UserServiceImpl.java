package hr.fer.diplomski.ms.taxi.billing.service.impl;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.billing.repository.UserRepository;
import hr.fer.diplomski.ms.taxi.billing.repository.model.User;
import hr.fer.diplomski.ms.taxi.billing.service.UserService;




@Service
public class UserServiceImpl implements UserService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private final UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	@Override
	public User findByPersonMsId(Long personMsId) {
		User user = null;
		try{
			user = userRepository.findByPersonMsId(personMsId);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity User not found exception by personMsId: {}", personMsId);
			return null;
		}
		LOGGER.info("Fetched User by personMsId: {} - {}", personMsId, user);
		if(user == null){
			return null;
		}
		return user;
	}


	@Override
	public User save(User user) {
		User savedUser = userRepository.save(user);
		LOGGER.info("Saved User - {}", user);
		if(user == null){
			return null;
		}
		return savedUser;	
	}

	

}
