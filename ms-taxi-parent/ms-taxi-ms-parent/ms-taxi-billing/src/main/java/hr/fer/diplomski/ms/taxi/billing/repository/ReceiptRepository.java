package hr.fer.diplomski.ms.taxi.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;

public interface ReceiptRepository extends JpaRepository<Receipt, Long>{
	
	Receipt findByTripId(String tripId);

}
