package hr.fer.diplomski.ms.taxi.billing.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tx_ms_bll_person")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)  
@DiscriminatorColumn(name = "person_type", discriminatorType = DiscriminatorType.STRING)  
public abstract class Person implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_ms_bll_person")
	@SequenceGenerator(name = "seq_tx_ms_bll_person", sequenceName = "seq_tx_ms_bll_person", allocationSize = 10)
	@Column(name = "per_id")
	protected Long id;
	
	@Column(name = "per_ms_id", nullable = false)
	protected Long personMsId;
	
	@Column(name = "per_first_name", nullable = false)
	protected String firstName;
	
	@Column(name = "per_last_name", nullable = false)
	protected String lastName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPersonMsId() {
		return personMsId;
	}

	public void setPersonMsId(Long personMsId) {
		this.personMsId = personMsId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
}
