package hr.fer.diplomski.ms.taxi.billing.service;

import hr.fer.diplomski.ms.taxi.billing.repository.model.User;

public interface UserService {
	
	User findByPersonMsId(Long personMsId);	
	
	User save(User user);
}
