package hr.fer.diplomski.ms.taxi.billing.repository.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("person_driver") 
public class Driver extends Person{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "driver_licence_number")
	private String licenceNumber;
	
	public Driver(){
		super();
	}
	
	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(licenceNumber);
		sb.append("]");
		return sb.toString();

	}
	
	
}
