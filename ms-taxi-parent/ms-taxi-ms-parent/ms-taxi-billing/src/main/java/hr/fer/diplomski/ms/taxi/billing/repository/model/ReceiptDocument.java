//package hr.fer.diplomski.ms.taxi.billing.repository.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "tx_ms_bll_receipt")
//public class ReceiptDocument implements Serializable{
//
//	private static final long serialVersionUID = -1859206203789212403L;
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_ms_bl_rec")
//	@SequenceGenerator(name = "seq_tx_ms_bl_rec", sequenceName = "seq_tx_ms_bl_rec", allocationSize = 1)
//	@Column(name = "rec_id", nullable = false)
//	private Long id;
//	
//	@Column(name = "rec_trip_id", nullable = false)
//	private Long tripId;
//
//	@Column(name = "rec_filename", nullable = false)
//	private String filename;
//	
//	@Column(name = "rec_content", nullable = false)
//	private byte[] content;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public Long getTripId() {
//		return tripId;
//	}
//
//	public void setTripId(Long tripId) {
//		this.tripId = tripId;
//	}
//
//	public String getFilename() {
//		return filename;
//	}
//
//	public void setFilename(String filename) {
//		this.filename = filename;
//	}
//
//	public byte[] getContent() {
//		return content;
//	}
//
//	public void setContent(byte[] content) {
//		this.content = content;
//	}
//	
//	public String toString(){
//		StringBuilder sb = new StringBuilder();
//		sb.append("[TripId: ");
//		sb.append(tripId);
//		sb.append(", filename: ");
//		sb.append(filename);
//		sb.append(" ]");
//		return sb.toString();
//	}	
//	
//}
