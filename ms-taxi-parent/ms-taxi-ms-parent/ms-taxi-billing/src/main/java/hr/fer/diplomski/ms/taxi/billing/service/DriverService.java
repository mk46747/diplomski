package hr.fer.diplomski.ms.taxi.billing.service;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Driver;

public interface DriverService {
	
	Driver findByPersonMsId(Long personMsId);	
	
	Driver save(Driver driver);

}
