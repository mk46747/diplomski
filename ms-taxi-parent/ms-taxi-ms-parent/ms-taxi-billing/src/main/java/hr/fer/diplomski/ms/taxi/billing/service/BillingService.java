package hr.fer.diplomski.ms.taxi.billing.service;

import hr.fer.diplomski.ms.taxi.billing.model.Trip;

public interface BillingService {
	
	void processTrip(Trip trip);

}
