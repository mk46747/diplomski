package hr.fer.diplomski.ms.taxi.billing.model;

import java.io.Serializable;


public class Taxi implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long driverId;

	private String registrationNumber;
	
	private String vehicleModel;
	
	private Address location;
	
	private TaxiStatus taxiStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}


	public TaxiStatus getTaxiStatus() {
		return taxiStatus;
	}

	public void setTaxiStatus(TaxiStatus taxiStatus) {
		this.taxiStatus = taxiStatus;
	}

	public Address getLocation() {
		return location;
	}

	public void setLocation(Address location) {
		this.location = location;
	} 
	
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", driverId: ");
		sb.append(driverId);
		sb.append(", ");
		sb.append(registrationNumber);
		sb.append(", ");
		sb.append(vehicleModel);
		sb.append(", ");
		sb.append(location);	
		sb.append(", ");
		sb.append(taxiStatus);
		sb.append("]");
		return sb.toString();
	}
	
	
	
}
