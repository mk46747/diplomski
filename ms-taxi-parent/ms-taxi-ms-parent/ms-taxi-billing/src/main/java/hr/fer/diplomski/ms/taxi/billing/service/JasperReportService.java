package hr.fer.diplomski.ms.taxi.billing.service;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;

public interface JasperReportService {

	byte[] generateReceiptPdf(Receipt receipt);
}
