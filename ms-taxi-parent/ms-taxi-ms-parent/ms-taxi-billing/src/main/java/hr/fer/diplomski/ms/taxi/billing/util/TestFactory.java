package hr.fer.diplomski.ms.taxi.billing.util;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;

public class TestFactory {

	public static java.util.Collection<Receipt> generateCollection() {
		final java.util.Vector<Receipt> collection = new java.util.Vector<Receipt>();
		collection.add(generateReceipt());

		return collection;

	}

	private static Receipt generateReceipt() {
		Receipt receipt = new Receipt();
		receipt.setTripId("1234");
		receipt.setDriverFirstName("Driver firstName");
		receipt.setDriverLastName("Driver lastName");
		receipt.setDriverLicenceNumber("000000000");
		receipt.setTaxiRegistrationNumber("ZG-500-FER");
		receipt.setTaxiVehicleModel("Renault 4");
		receipt.setTripBaseFare("20 kn");
		receipt.setTripDestinationAddress("Unska 3, Zagreb, Hrvatska");
		receipt.setTripDistance("13.5 km");
		receipt.setTripDistanceFare("11.2 kn");
		receipt.setTripDuration("11 min");
		receipt.setTripEndTime("08.02.2019. 12:00");
		receipt.setTripStartAddress("Lastovska 23, Zagreb, Hrvatska");
		receipt.setTripStartTime("08.02.2019. 11:49");
		receipt.setTripTimeFare("12 kn");
		receipt.setTripTotalFare("43.2 kn");
		receipt.setUserCreditCard("0000 0000 0000 0000");
		receipt.setUserEmail("user@test.com");
		receipt.setUserFirstName("User firstname");
		receipt.setUserLastName("user lastName");
		return receipt;
		
	}
}
