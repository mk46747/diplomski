//package hr.fer.diplomski.ms.taxi.billing.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//import hr.fer.diplomski.ms.taxi.billing.repository.model.ReceiptDocument;
//
//@Repository
//public interface ReceiptDocumentRepository extends JpaRepository<ReceiptDocument, Long> {
//
//	ReceiptDocument findByTripId(Long tripId);
//
//}
