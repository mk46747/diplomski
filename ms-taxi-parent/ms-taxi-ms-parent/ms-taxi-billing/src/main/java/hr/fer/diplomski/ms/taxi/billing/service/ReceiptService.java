package hr.fer.diplomski.ms.taxi.billing.service;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;

public interface ReceiptService {

	Receipt fetchReceipt(String tripId);
	
	Receipt saveReceipt(Receipt receiptDocument);
	
	byte[] generatePdfReceipt(String tripId);

	byte[] generatePdfReceipt(Receipt receipt);
		
}
