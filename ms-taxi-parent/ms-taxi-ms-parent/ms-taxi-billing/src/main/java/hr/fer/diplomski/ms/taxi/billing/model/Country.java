package hr.fer.diplomski.ms.taxi.billing.model;

import java.io.Serializable;

public class Country implements Serializable {

	private static final long serialVersionUID = -1499689578514553038L;

	private Long id;

	private String name;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(name);
		sb.append("]");
		return sb.toString();
	}
	
	
}
