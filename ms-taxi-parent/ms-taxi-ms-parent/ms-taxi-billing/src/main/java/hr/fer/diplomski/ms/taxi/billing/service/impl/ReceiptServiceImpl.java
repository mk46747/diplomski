package hr.fer.diplomski.ms.taxi.billing.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.billing.repository.ReceiptRepository;
import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;
import hr.fer.diplomski.ms.taxi.billing.service.JasperReportService;
import hr.fer.diplomski.ms.taxi.billing.service.ReceiptService;



@Service
public class ReceiptServiceImpl implements ReceiptService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(ReceiptServiceImpl.class);

	
	private final ReceiptRepository receiptRepository;
	
	private final JasperReportService jasperReportService;

	
	@Autowired
	public ReceiptServiceImpl(final ReceiptRepository receiptRepository, JasperReportService jasperReportService) {
		this.receiptRepository = receiptRepository;
		this.jasperReportService = jasperReportService;
	}

	@Override
	public Receipt fetchReceipt(String tripId) {
		LOGGER.info("Fetching receipt document dor tripId: {}", tripId);
		Receipt receipt = null;
		try{
			receipt = receiptRepository.findByTripId(tripId);
		}catch(Exception e){
			LOGGER.error("Exception happened while fetching receipt for tripId: {}", tripId, e);
		}
		LOGGER.info("Fetched receipt for tripId: {} - {}", tripId, receipt);
		if(receipt == null){
			return null;
		}
		return receipt;
	}

	@Override
	public Receipt saveReceipt(Receipt receipt) {
		LOGGER.info("Saving receipt: {}", receipt);
		Receipt savedReceipt = receiptRepository.save(receipt);
		LOGGER.info("Saved Receipt: {}", savedReceipt);
		return savedReceipt;

	}

	@Override
	public byte[] generatePdfReceipt(String tripId) {
		Receipt receipt = fetchReceipt(tripId);
		return generatePdfReceipt(receipt);
	}

	@Override
	public byte[] generatePdfReceipt(Receipt receipt) {
		LOGGER.info("Invoking jasper generate receipt for receipt: {}", receipt);
		byte[] receiptPdf = jasperReportService.generateReceiptPdf(receipt);
		LOGGER.info("Generated pdf receipt, size: {} bytes", receiptPdf == null ? 0 : receiptPdf.length);
		return receiptPdf;

	}

}
