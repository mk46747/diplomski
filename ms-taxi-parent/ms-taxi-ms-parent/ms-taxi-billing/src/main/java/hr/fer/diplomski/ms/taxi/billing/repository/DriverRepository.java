package hr.fer.diplomski.ms.taxi.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Driver;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long>{

	Driver findByPersonMsId(Long personMsId);

}
