package hr.fer.diplomski.ms.taxi.billing.service.impl;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.billing.repository.DriverRepository;
import hr.fer.diplomski.ms.taxi.billing.repository.model.Driver;
import hr.fer.diplomski.ms.taxi.billing.service.DriverService;

@Service
public class DriverServiceImpl implements DriverService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(DriverServiceImpl.class);
	
	private final DriverRepository driverRepository;
	
	@Autowired
	public DriverServiceImpl(DriverRepository driverRepository) {
		this.driverRepository = driverRepository;
	}


	@Override
	public Driver findByPersonMsId(Long personMsId) {
		Driver driver = null;
		try{
			driver = driverRepository.findByPersonMsId(personMsId);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity Driver not found exception by personMsId: {}", personMsId);
			return null;
		}
		LOGGER.info("Fetched Driver by personMsId: {} - {}", personMsId, driver);
		if(driver == null){
			return null;
		}
		return driver;
	}


	@Override
	public Driver save(Driver driver) {
		Driver savedDriver = driverRepository.save(driver);
		LOGGER.info("Saved Driver - {}", driver);
		if(driver == null){
			return null;
		}
		return savedDriver;	
	}

}
