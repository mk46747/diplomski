package hr.fer.diplomski.ms.taxi.billing.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tx_ms_bll_receipt")
public class Receipt implements Serializable{

	private static final long serialVersionUID = -5778542532131199104L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_ms_bl_rec")
	@SequenceGenerator(name = "seq_tx_ms_bl_rec", sequenceName = "seq_tx_ms_bl_rec", allocationSize = 1)
	@Column(name = "rec_id", nullable = false)	
	private long receiptId;
	
	@Column(name = "rec_trip_id", nullable = false)
	private String tripId;
	
	@Column
	private String userFirstName;
	
	@Column
	private String userLastName;
	@Column
	private String userEmail;
	@Column
	private String userCreditCard;
	
	@Column
	private String driverFirstName;
	@Column
	private String driverLastName;
	@Column
	private String driverLicenceNumber;
	
	@Column
	private String taxiRegistrationNumber;
	@Column
	private String taxiVehicleModel;
	
	@Column
	private String tripStartAddress;
	@Column
	private String tripDestinationAddress;
	
	@Column
	private String tripStartTime;
	@Column
	private String tripEndTime;
	
	@Column
	private String tripDistance;
	@Column
	private String tripDuration;
	
	@Column
	private String tripBaseFare;
	@Column
	private String tripTimeFare;
	@Column
	private String tripDistanceFare;
	@Column
	private String tripTotalFare;
	
	
	public long getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserCreditCard() {
		return userCreditCard;
	}
	public void setUserCreditCard(String userCreditCard) {
		this.userCreditCard = userCreditCard;
	}
	public String getDriverFirstName() {
		return driverFirstName;
	}
	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}
	public String getDriverLastName() {
		return driverLastName;
	}
	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}
	public String getDriverLicenceNumber() {
		return driverLicenceNumber;
	}
	public void setDriverLicenceNumber(String driverLicenceNumber) {
		this.driverLicenceNumber = driverLicenceNumber;
	}
	public String getTaxiRegistrationNumber() {
		return taxiRegistrationNumber;
	}
	public void setTaxiRegistrationNumber(String taxiRegistrationNumber) {
		this.taxiRegistrationNumber = taxiRegistrationNumber;
	}
	public String getTaxiVehicleModel() {
		return taxiVehicleModel;
	}
	public void setTaxiVehicleModel(String taxiVehicleModel) {
		this.taxiVehicleModel = taxiVehicleModel;
	}
	public String getTripStartAddress() {
		return tripStartAddress;
	}
	public void setTripStartAddress(String tripStartAddress) {
		this.tripStartAddress = tripStartAddress;
	}
	public String getTripDestinationAddress() {
		return tripDestinationAddress;
	}
	public void setTripDestinationAddress(String tripDestinationAddress) {
		this.tripDestinationAddress = tripDestinationAddress;
	}
	public String getTripStartTime() {
		return tripStartTime;
	}
	public void setTripStartTime(String tripStartTime) {
		this.tripStartTime = tripStartTime;
	}
	public String getTripEndTime() {
		return tripEndTime;
	}
	public void setTripEndTime(String tripEndTime) {
		this.tripEndTime = tripEndTime;
	}
	public String getTripDistance() {
		return tripDistance;
	}
	public void setTripDistance(String tripDistance) {
		this.tripDistance = tripDistance;
	}
	public String getTripDuration() {
		return tripDuration;
	}
	public void setTripDuration(String tripDuration) {
		this.tripDuration = tripDuration;
	}
	public String getTripBaseFare() {
		return tripBaseFare;
	}
	public void setTripBaseFare(String tripBaseFare) {
		this.tripBaseFare = tripBaseFare;
	}
	public String getTripTimeFare() {
		return tripTimeFare;
	}
	public void setTripTimeFare(String tripTimeFare) {
		this.tripTimeFare = tripTimeFare;
	}
	public String getTripDistanceFare() {
		return tripDistanceFare;
	}
	public void setTripDistanceFare(String tripDistanceFare) {
		this.tripDistanceFare = tripDistanceFare;
	}
	public String getTripTotalFare() {
		return tripTotalFare;
	}
	public void setTripTotalFare(String tripTotalFare) {
		this.tripTotalFare = tripTotalFare;
	}
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[User firstName: ");
		sb.append(userFirstName);
		sb.append(", total fare: ");
		sb.append(tripTotalFare);
		sb.append(" ... ]");
		return sb.toString();
	}
	
}
