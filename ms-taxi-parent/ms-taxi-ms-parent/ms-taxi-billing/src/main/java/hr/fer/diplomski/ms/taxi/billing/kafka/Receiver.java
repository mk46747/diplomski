package hr.fer.diplomski.ms.taxi.billing.kafka;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import hr.fer.diplomski.ms.taxi.billing.model.Trip;
import hr.fer.diplomski.ms.taxi.billing.repository.model.Driver;
import hr.fer.diplomski.ms.taxi.billing.repository.model.User;
import hr.fer.diplomski.ms.taxi.billing.service.BillingService;
import hr.fer.diplomski.ms.taxi.billing.service.DriverService;
import hr.fer.diplomski.ms.taxi.billing.service.UserService;




@Component
public class Receiver {
	
	private final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);
	
	private CountDownLatch latch = new CountDownLatch(1);
	@Autowired
	private UserService userService;
	@Autowired
	private DriverService driverService;
	@Autowired
	private BillingService billingService;
	
//	@Autowired
//	public Receiver(final UserService userService, final DriverService driverService, final BillingService billingService){
//		this.userService = userService;
//		this.driverService = driverService;
//		this.billingService = billingService;
//	}
	
	public CountDownLatch getLatch(){
		return latch;
	}

    @KafkaListener(topics = "${spring.kafka.topic.user-topic}")
	public void receiveUser(KafkaUser kafkaUser){
    	LOGGER.info("Received user payload: {}", kafkaUser);
    	//TODO
    	latch.countDown();
    	
    	User user = userService.findByPersonMsId(kafkaUser.getUserId());
    	
    	if(user == null){
    		user = new User();
    		LOGGER.info("User with personMsId: {} does not exist, creating new user", kafkaUser.getUserId());
    	}else{
    		LOGGER.info("Updating user with personMsId: {}", kafkaUser.getUserId());
    	}
    	
    	user.setPersonMsId(kafkaUser.getUserId());
    	user.setFirstName(kafkaUser.getUserFirstName());
    	user.setLastName(kafkaUser.getUserLastName());
    	user.setCreditCardNumber(kafkaUser.getUserCreditCardNumber());
    	user.setEmail(kafkaUser.getEmail());
    	
    	userService.save(user);    	
	}
    
    @KafkaListener(topics = "${spring.kafka.topic.driver-topic}")
	public void receiveUser(KafkaDriver kafkaDriver){
    	LOGGER.info("Received driver payload: {}", kafkaDriver);
    	//TODO
    	latch.countDown();
    	
    	Driver driver = driverService.findByPersonMsId(kafkaDriver.getDriverId());
    	
    	if(driver == null){
    		driver = new Driver();
    		LOGGER.info("Driver with personMsId: {} does not exist, creating new driver");
    	}else{
    		LOGGER.info("Updating driver with personMsId: {}");
    	}
    	
    	driver.setPersonMsId(kafkaDriver.getDriverId());
    	driver.setFirstName(kafkaDriver.getDriverFirstName());
    	driver.setLastName(kafkaDriver.getDriverLastName());
    	driver.setLicenceNumber(kafkaDriver.getDriverLicenceNumber());
    	
    	driverService.save(driver);    	
	}
    
    @KafkaListener(topics = "${spring.kafka.topic.trip-topic}")
	public void receiveTrip(Trip trip){
    	LOGGER.info("Received trip payload: {}", trip);
    	LOGGER.info("Invoking billing...");
    	billingService.processTrip(trip);
    	
    }
}
