package hr.fer.diplomski.ms.taxi.billing.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.billing.repository.model.Receipt;
import hr.fer.diplomski.ms.taxi.billing.service.JasperReportService;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class JasperReportServiceImpl implements JasperReportService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(JasperReportServiceImpl.class);
	
	private final String JASPER_RECEIPT_TEMPLATE = "jasper/receipt.jasper";
	
	public final String JASPER_PARAM_NAME_RECEIPT = "receipt";


	@Override
	public byte[] generateReceiptPdf(Receipt receipt) {
		if(receipt == null){
			return null;
		}
		LOGGER.info("Generating pdf receipt from receipt: {}", receipt);
		
		final Resource resource = new ClassPathResource(JASPER_RECEIPT_TEMPLATE);
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put(JASPER_PARAM_NAME_RECEIPT, receipt);	
		
		try(InputStream inputStream = new FileInputStream(resource.getFile())){
			
			final java.util.Vector<Receipt> receipts = new java.util.Vector<Receipt>();
			receipts.add(receipt);
			
			final JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, params, new JRBeanCollectionDataSource(receipts));
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, byteArrayOutputStream);
			return byteArrayOutputStream.toByteArray();
			
		}catch (final Exception e){
			LOGGER.error("Error while generating receipt pdf", e);
			return null;
		}


	}

}
