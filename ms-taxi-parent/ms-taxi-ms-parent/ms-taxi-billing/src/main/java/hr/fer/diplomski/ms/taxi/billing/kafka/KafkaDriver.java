package hr.fer.diplomski.ms.taxi.billing.kafka;

import java.io.Serializable;

public class KafkaDriver implements Serializable{
	
	private static final long serialVersionUID = 8686086384135105661L;

	private Long driverId;
	
	private String driverFirstName;
	
	private String driverLastName;
	
	private String driverLicenceNumber;

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getDriverFirstName() {
		return driverFirstName;
	}

	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}

	public String getDriverLastName() {
		return driverLastName;
	}

	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}
	
	
	
	public String getDriverLicenceNumber() {
		return driverLicenceNumber;
	}

	public void setDriverLicenceNumber(String driverLicenceNumber) {
		this.driverLicenceNumber = driverLicenceNumber;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[driverId:");
		sb.append(driverId);
		sb.append(", driverFirstName: ");
		sb.append(driverFirstName);
		sb.append(", driverLastName: ");		
		sb.append(driverLastName);
		sb.append(", driverLicenceNumber: ");		
		sb.append(driverLicenceNumber);
		sb.append("]");
		return sb.toString();

	}
	
	
}
