package hr.fer.diplomski.ms.taxi.user.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import hr.fer.diplomski.ms.taxi.user.kafka.DriverAfterSaveListener;

@Entity
@EntityListeners(DriverAfterSaveListener.class)
@DiscriminatorValue("person_driver") 
public class Driver extends Person{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "driver_licence_number")
	private String licenceNumber;
	
	public Driver(){
		super();
	}
	
	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(personId);
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(licenceNumber);
		sb.append("]");
		return sb.toString();

	}
	
	
}
