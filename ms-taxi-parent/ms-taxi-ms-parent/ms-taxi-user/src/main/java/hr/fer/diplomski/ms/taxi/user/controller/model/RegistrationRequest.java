package hr.fer.diplomski.ms.taxi.user.controller.model;


import javax.validation.constraints.NotBlank;

public class RegistrationRequest {
	
    @NotBlank
	private String firstName;
    
    @NotBlank
	private String lastName;
    
    @NotBlank
	private String email;
    
    @NotBlank
	private String phoneNumber;
    
    @NotBlank
	private String username;
    
    @NotBlank
	private String password;
    
    @NotBlank
	private String creditCardNumber;


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(username);
		sb.append(", ");		
		sb.append(password);
		sb.append("]");
		return sb.toString();

	}
	
	
	
}
