package hr.fer.diplomski.ms.taxi.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.user.model.Driver;


@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

}
