package hr.fer.diplomski.ms.taxi.user.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUserDetails extends User{
	
	private static final long serialVersionUID = -8508999888782475397L;
	
	protected Long userId;
	
	public CustomUserDetails(Long userId, String username, String password, Collection<? extends GrantedAuthority> authorities){
		super(username, password, authorities);
		this.userId = userId;
	}

	public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	

	

}
