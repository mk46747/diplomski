package hr.fer.diplomski.ms.taxi.user.kafka;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import hr.fer.diplomski.ms.taxi.user.model.User;

public class UserAfterSaveListener{
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserAfterSaveListener.class);
	
    @Value("${spring.kafka.topic.user-topic}")
	private String topic;
    
    private Sender sender;
    
    @Autowired
    public UserAfterSaveListener(Sender sender){
    	this.sender = sender;
    }

	@PostPersist
	@PostUpdate
	void SendUserToKafka(User user){
		LOGGER.info("Invoked persistion listener on User: {}", user);
		hr.fer.diplomski.ms.taxi.user.kafka.KafkaUser kafkaUser = new hr.fer.diplomski.ms.taxi.user.kafka.KafkaUser();
		kafkaUser.setUserId(user.getPersonId());
		kafkaUser.setUserFirstName(user.getFirstName());
		kafkaUser.setUserLastName(user.getLastName());
		kafkaUser.setUserCreditCardNumber(user.getCreditCardNumber());
		kafkaUser.setEmail(user.getEmail());
		
		LOGGER.info("Sending kafkaUser: {} to kafka", kafkaUser);
		sender.send(topic, kafkaUser);
	}
}
