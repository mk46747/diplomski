package hr.fer.diplomski.ms.taxi.user.service.impl;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.user.model.User;
import hr.fer.diplomski.ms.taxi.user.repository.UserRepository;
import hr.fer.diplomski.ms.taxi.user.service.UserService;



@Service
public class UserServiceImpl implements UserService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private final UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User findByUsername(String username) {
		User user = null;
		try{
			user = userRepository.findByUsername(username);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity User not found exception by username: {}", username);
			return null;
		}
		LOGGER.info("Fetched User by username: {} - {}", username, user);
		if(user == null){
			return null;
		}
		return user;
	}
	

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		User user = null;
		try{
			user = userRepository.findByUsernameAndPassword(username, password);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity User not found exception by username: {}, password: {}", username, password);
			return null;
		}
		LOGGER.info("Fetched User by username: {} and password, {} - {}", username, password, user);
		if(user == null){
			return null;
		}
		return user;
	}

	@Override
	public User findById(Long id) {
		User user = null;
		try{
			user = userRepository.findById(id).orElse(null);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity User not found exception by id: {}", id);
			return null;
		}
		LOGGER.info("Fetched User by id: {} - {}", id, user);
		if(user == null){
			return null;
		}
		return user;
	}

	@Override
	public User create(User user) {
		User createdUser = userRepository.save(user);
		LOGGER.info("Saved User - {}", user);
		if(user == null){
			return null;
		}
		return createdUser;

	}

	@Override
	public User update(User user) {
		User updatedUser = userRepository.save(user);
		LOGGER.info("Saved User - {}", user);
		if(user == null){
			return null;
		}
		return updatedUser;	
	}

	@Override
	public void delete(Long id) {
		userRepository.deleteById(id);
	}

	@Override
	public boolean existsByUsername(String username) {
		return userRepository.existsByUsername(username);
	}

	
	

}
