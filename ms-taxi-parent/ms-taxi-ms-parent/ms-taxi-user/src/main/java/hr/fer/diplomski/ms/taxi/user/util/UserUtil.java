package hr.fer.diplomski.ms.taxi.user.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ch.qos.logback.core.encoder.Encoder;
import hr.fer.diplomski.ms.taxi.user.model.User;

public class UserUtil {
	
public static int USERS_NUMBER = 500;
	
	public static final String USER_FIRST_NAME_PREFIX = "first_name_";
	public static final String USER_LAST_NAME_PREFIX = "last_name_";
	public static final String USER_EMAIL_PREFIX = "email_";
	public static final String USER_PHONE_NUMBER_PREFIX = "phone_number_";
	public static final String USER_USERNAME = "username_";
	public static final String USER_PASSWORD = "password_";
	public static final String USER_CREDIT_CARD = "0000 0000 0000 ";
	
	private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(16);

    
	public static User generateUser(int i){

        User user = new User();
        user.setFirstName(USER_FIRST_NAME_PREFIX + i);
        user.setLastName(USER_LAST_NAME_PREFIX + i);
        user.setEmail(USER_EMAIL_PREFIX + i);
        user.setPhoneNumber(USER_PHONE_NUMBER_PREFIX + i);
        user.setUsername(USER_USERNAME + i);
        user.setPassword("$2a$16$zXPCl79dTA2sKM2RBOpmauq3evYkAAZRoJL8qY.KCZwzi1/y1/.xe");
        user.setCreditCardNumber(USER_CREDIT_CARD + i);
        
        return user;
        
    }
}
