package hr.fer.diplomski.ms.taxi.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.user.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
		
	User findByUsername(String username);
	
	User findByUsernameAndPassword(String username, String password);
	
	boolean existsByUsername(String username);
			
}
