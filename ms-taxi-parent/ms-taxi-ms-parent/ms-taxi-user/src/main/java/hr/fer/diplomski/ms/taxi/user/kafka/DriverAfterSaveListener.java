package hr.fer.diplomski.ms.taxi.user.kafka;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import hr.fer.diplomski.ms.taxi.user.model.Driver;


public class DriverAfterSaveListener {
private final Logger LOGGER = LoggerFactory.getLogger(DriverAfterSaveListener.class);
	
    @Value("${spring.kafka.topic.driver-topic}")
	private String topic;
    
    private Sender sender;
    
    @Autowired
    public DriverAfterSaveListener(Sender sender){
    	this.sender = sender;
    }

	@PostPersist
	@PostUpdate
	void SendDriverToKafka(Driver driver){
		LOGGER.info("Invoked persistance listener on Driver: {}", driver);
		hr.fer.diplomski.ms.taxi.user.kafka.KafkaDriver kafkaDriver = new hr.fer.diplomski.ms.taxi.user.kafka.KafkaDriver();
		kafkaDriver.setDriverId(driver.getPersonId());
		kafkaDriver.setDriverFirstName(driver.getFirstName());
		kafkaDriver.setDriverLastName(driver.getLastName());
		kafkaDriver.setDriverLicenceNumber(driver.getLicenceNumber());
		
		LOGGER.info("Sending kafkaDriver: {} to kafka", kafkaDriver);
		sender.send(topic, kafkaDriver);
	}
}
