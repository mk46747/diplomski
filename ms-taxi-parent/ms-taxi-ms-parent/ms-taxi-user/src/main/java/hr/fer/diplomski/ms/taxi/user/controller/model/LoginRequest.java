package hr.fer.diplomski.ms.taxi.user.controller.model;


import javax.validation.constraints.NotBlank;

public class LoginRequest {
    
    @NotBlank
	private String username;
    
    @NotBlank
	private String password;
    

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
	
		sb.append(username);
		sb.append(", ");		
		sb.append(password);
		sb.append("]");
		return sb.toString();

	}
	
	
	
}
