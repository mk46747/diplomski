package hr.fer.diplomski.ms.taxi.user.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;


public class Sender {

	private final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

	@Autowired
	private KafkaTemplate<String, KafkaUser> kafkaUserTemplate;
	
	@Autowired
	private KafkaTemplate<String, KafkaDriver> kafkaDriverTemplate;
	
	public void send(String topic, KafkaUser payload){
		LOGGER.info("Sending payload: {} to topic: {}", payload, topic);
		
		kafkaUserTemplate.send(topic, payload);
	}
	
	public void send(String topic, KafkaDriver payload){
		LOGGER.info("Sending payload: {} to topic: {}", payload, topic);
		
		kafkaDriverTemplate.send(topic, payload);
	}
}
