package hr.fer.diplomski.ms.taxi.user;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;


import hr.fer.diplomski.ms.taxi.user.model.Driver;
import hr.fer.diplomski.ms.taxi.user.model.User;
import hr.fer.diplomski.ms.taxi.user.repository.DriverRepository;
import hr.fer.diplomski.ms.taxi.user.repository.UserRepository;
import hr.fer.diplomski.ms.taxi.user.util.DriverUtil;
import hr.fer.diplomski.ms.taxi.user.util.UserUtil;



@Component
public class Initializer implements CommandLineRunner {
	
	private final Logger LOGGER = LoggerFactory.getLogger(Initializer.class);
	
	private static final int DRIVER_NUMBER = 12000;
	private static final int USER_NUMBER = 10000;

	@SuppressWarnings("unused")
	private final UserRepository userRepository;
	
	private final DriverRepository driverRepository;
	
	@SuppressWarnings("unused")
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	
	public Initializer(UserRepository userRepository, DriverRepository driverRepository) {
		this.userRepository = userRepository;
		this.driverRepository = driverRepository;
	}

	@Override
	public void run(String... args) throws Exception {
//		User user = new User();
//		user.setEmail("marko@mrzljak.beba");
//		user.setFirstName("marko");
//		user.setLastName("mrzljak");
//		user.setPassword(encoder.encode("beba"));
//		user.setPhoneNumber("000000000000");
//		user.setRole(Role.ROLE_USER);
//		user.setUsername("markobebamrzljak");
//		LOGGER.info("saving user: {}", user);
//		userRepository.save(user);
		
		LOGGER.info("Generating {} Driver objects...", DRIVER_NUMBER);
		List<Driver> driverList = new ArrayList<Driver>();
		for(int i = 0; i < DRIVER_NUMBER; i++){
			Driver driver = DriverUtil.generateDriver(i);
			driverList.add(driver);
		}
		LOGGER.info("Saving Driver objects.");
		driverRepository.saveAll(driverList);		
		LOGGER.info("Taxi objects saved.");
		
//		LOGGER.info("Generating {} User objects...", USER_NUMBER);
//		List<User> userList = new ArrayList<User>();
//		for(int i = 0; i < USER_NUMBER; i++){
//			User user = UserUtil.generateUser(i);
//			userList.add(user);
//		}
//		LOGGER.info("Saving User objects.");
//		userRepository.saveAll(userList);		
//		LOGGER.info("User objects saved.");

	}
	
	

}
