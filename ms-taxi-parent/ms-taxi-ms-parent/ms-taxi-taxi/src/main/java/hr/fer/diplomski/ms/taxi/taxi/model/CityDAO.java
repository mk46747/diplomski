package hr.fer.diplomski.ms.taxi.taxi.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import hr.fer.diplomski.taxi.service.model.City;

@Entity
@Table(name = "tx_city")
public class CityDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_city")
	@SequenceGenerator(name = "seq_tx_city", sequenceName = "seq_tx_city", allocationSize = 1)
	@Column(name = "city_id", nullable = false)
	private Long id;

	@Column(name = "city_name", nullable = false)
	private String name;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "country_id")
	private CountryDAO country;
	
	public CityDAO (City city){
		if(city == null){
			return;
		}
		this.setId(city.getId());
		if(city.getCountry() != null){
			this.setCountry(new CountryDAO(city.getCountry()));
		}
		this.setName(city.getName());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CountryDAO getCountry() {
		return country;
	}

	public void setCountry(CountryDAO country) {
		this.country = country;
	}
	
	public City convertFromDAO(){
		City city = new City();	
		city.setId(this.getId());
		if(country != null){
			city.setCountry(country.convertFromDAO());
		}
		city.setName(this.getName());
		return city;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(name);
		sb.append("]");
		return sb.toString();
	}
	
	
	
	
}
