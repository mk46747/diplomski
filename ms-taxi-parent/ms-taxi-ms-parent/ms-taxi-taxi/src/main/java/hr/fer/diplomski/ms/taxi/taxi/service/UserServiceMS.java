package hr.fer.diplomski.ms.taxi.taxi.service;

import hr.fer.diplomski.taxi.service.model.Driver;

public interface UserServiceMS {
		
	Driver getDriver(Long id);
	
}
