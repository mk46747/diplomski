package hr.fer.diplomski.ms.taxi.taxi.service.impl;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.ms.taxi.taxi.model.AddressDAO;
import hr.fer.diplomski.ms.taxi.taxi.model.TaxiDAO;
import hr.fer.diplomski.ms.taxi.taxi.repository.TaxiRepository;
import hr.fer.diplomski.ms.taxi.taxi.service.TaxiService;
import hr.fer.diplomski.ms.taxi.taxi.service.UserServiceMS;
import hr.fer.diplomski.taxi.service.model.Address;
import hr.fer.diplomski.taxi.service.model.Driver;
import hr.fer.diplomski.taxi.service.model.Taxi;
import hr.fer.diplomski.taxi.service.model.TaxiStatus;

@Service
public class TaxiServiceImpl implements TaxiService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(TaxiServiceImpl.class);

	
	private final TaxiRepository taxiRepository;
	
	private final UserServiceMS userServiceMS;
	
	@Autowired
	public TaxiServiceImpl(final TaxiRepository taxiRepository, final UserServiceMS userServiceMS) {
		this.taxiRepository = taxiRepository;
		this.userServiceMS = userServiceMS;
	}

	@Override
	public Long findTaxi(Address start) {
		TaxiDAO taxiDAO = null;
		try{
			taxiDAO =  taxiRepository.findFirstByTaxiStatus(TaxiStatus.FREE);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity TaxiDAO not found exception for status:{}", TaxiStatus.FREE);
			//TODO
			return null;
		}
		 
		LOGGER.info("Fetched TaxiDAO by address: {} - {}",start, taxiDAO);
		if(taxiDAO == null){
			return null;
		}
		return taxiDAO.getId();
	}

	@Override
	public void updateTaxiEndTrip(Long taxiId, Address destination) {
		LOGGER.info("Saving taxi end - id: {},  location: {}", taxiId, destination);
		TaxiDAO taxi = getOneTaxi(taxiId);
		if(taxi == null){
			LOGGER.error("No taxi found for id: {}", taxiId);
			//TODO exception
			return;
		}
		taxi.setLocation(new AddressDAO(destination));
		taxi.setTaxiStatus(TaxiStatus.FREE);
		TaxiDAO taxiDAO =  taxiRepository.save(taxi);
		LOGGER.info("Saved TaxiDAO: {}", taxiDAO);		
	}

	@Override
	public void updateTaxiStartTrip(Long taxiId, Address start) {
		LOGGER.info("Saving taxi start - id: {},  location: {}", taxiId, start);
		TaxiDAO taxi = getOneTaxi(taxiId);
		if(taxi == null){
			LOGGER.error("No taxi found for id: {}", taxiId);
			//TODO exception
			return;
		}
		taxi.setLocation(new AddressDAO(start));
		taxi.setTaxiStatus(TaxiStatus.BUSY);		
		TaxiDAO taxiDAO =  taxiRepository.save(taxi);
		LOGGER.info("Saved TaxiDAO: {}", taxiDAO);
		
	}
	
	@Override
	public Taxi getTaxi(Long id) {
		TaxiDAO taxiDAO =  getOneTaxi(id);

		LOGGER.info("Fetched TaxiDAO by id: {} - {}", id, taxiDAO);
		if(taxiDAO == null){
			return null;
		}
		Driver driver = userServiceMS.getDriver(taxiDAO.getDriverId());
		LOGGER.info("Fetched Driver by id: {} - {}", taxiDAO.getDriverId(), driver);

		Taxi taxi = taxiDAO.convertFromDAO();
		taxi.setDriver(driver);
		return taxi;
	}
	
	private TaxiDAO getOneTaxi(Long id){
		TaxiDAO taxiDAO = null;
		try{
			taxiDAO = taxiRepository.getOne(id);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity TaxiDAO not found exception for id:{}", id);
			return null;
		}
		return taxiDAO;
	}

}
