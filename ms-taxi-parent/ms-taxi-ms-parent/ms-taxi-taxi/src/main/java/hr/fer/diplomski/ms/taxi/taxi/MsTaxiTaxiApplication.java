package hr.fer.diplomski.ms.taxi.taxi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MsTaxiTaxiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsTaxiTaxiApplication.class, args);
	}

}

