package hr.fer.diplomski.ms.taxi.taxi.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import hr.fer.diplomski.taxi.service.model.Taxi;
import hr.fer.diplomski.taxi.service.model.TaxiStatus;

@Entity
@Table(name = "tx_ms_tx_taxi")
public class TaxiDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_ms_tx_taxi")
	@SequenceGenerator(name = "seq_tx_ms_tx_taxi", sequenceName = "seq_tx_ms_tx_taxi", allocationSize = 1)
	@Column(name = "taxi_id", nullable = false)
	private Long id;
	
	@Column(name = "driver_id", nullable = false)
	private Long driverId;

	@Column(name = "taxi_reg_number", nullable = false)
	private String registrationNumber;
	
	@Column(name = "taxi_vehicle_model", nullable = false)
	private String vehicleModel;
	
	@OneToOne(cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "taxi_address_id")
	private AddressDAO location;
	
	@Enumerated(EnumType.STRING)
	private TaxiStatus taxiStatus;
	
	
	public TaxiDAO(Taxi taxi){
		if(taxi == null){
			return;
		}
		if(taxi.getDriver() != null){
			this.setDriverId(taxi.getDriver().getId());
		}
		this.setId(taxi.getId());
		if(taxi.getLocation() != null){
			this.setLocation(new AddressDAO(taxi.getLocation()));
		}
		this.setRegistrationNumber(taxi.getRegistrationNumber());
		this.setTaxiStatus(taxi.getTaxiStatus());
		this.setVehicleModel(taxi.getVehicleModel());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}


	public TaxiStatus getTaxiStatus() {
		return taxiStatus;
	}

	public void setTaxiStatus(TaxiStatus taxiStatus) {
		this.taxiStatus = taxiStatus;
	}

	public AddressDAO getLocation() {
		return location;
	}

	public void setLocation(AddressDAO location) {
		this.location = location;
	} 
	
	/**
	 * Driver needs to be set later
	 * @return
	 */
	public Taxi convertFromDAO(){
		Taxi taxi = new Taxi();

		//taxi.setDriverId(taxiDAO.getDriver().getId());
		taxi.setId(this.getId());
		if(this.getLocation() != null){
			taxi.setLocation(this.location.convertFromDAO());
		}
		taxi.setRegistrationNumber(this.getRegistrationNumber());
		taxi.setTaxiStatus(this.getTaxiStatus());
		taxi.setVehicleModel(this.getVehicleModel());
		return taxi;
		
		
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", driverId: ");
		sb.append(driverId);
		sb.append(", ");
		sb.append(registrationNumber);
		sb.append(", ");
		sb.append(vehicleModel);
		sb.append(", ");
		sb.append(location);	
		sb.append(", ");
		sb.append(taxiStatus);
		sb.append("]");
		return sb.toString();
	}
	
	
	
}
