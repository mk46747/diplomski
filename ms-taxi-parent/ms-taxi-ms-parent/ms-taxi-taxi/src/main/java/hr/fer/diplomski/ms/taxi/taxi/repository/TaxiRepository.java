package hr.fer.diplomski.ms.taxi.taxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.ms.taxi.taxi.model.TaxiDAO;
import hr.fer.diplomski.taxi.service.model.TaxiStatus;



@Repository
public interface TaxiRepository extends JpaRepository<TaxiDAO, Long>{

	TaxiDAO findFirstByTaxiStatus(TaxiStatus taxiStatus);
}
