package hr.fer.diplomski.ms.taxi.taxi.service;

import hr.fer.diplomski.taxi.service.model.Address;
import hr.fer.diplomski.taxi.service.model.Taxi;

public interface TaxiService {

	Long findTaxi(Address start);

	void updateTaxiEndTrip(Long taxiId, Address destination);

	void updateTaxiStartTrip(Long taxiId, Address start);
	
	Taxi getTaxi(Long id);
}
