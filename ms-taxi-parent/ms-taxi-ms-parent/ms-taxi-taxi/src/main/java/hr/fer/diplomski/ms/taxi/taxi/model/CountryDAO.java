package hr.fer.diplomski.ms.taxi.taxi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import hr.fer.diplomski.taxi.service.model.Country;
@Entity
@Table(name = "tx_country")
public class CountryDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_cuntry")
	@SequenceGenerator(name = "seq_tx_cuntry", sequenceName = "seq_tx_cuntry", allocationSize = 1)
	@Column(name = "country_id", nullable = false)
	private Long id;

	@Column(name = "country_name", nullable = false)
	private String name;
	
	public CountryDAO (Country country){
		if(country == null){
			return;
		}
		this.setId(country.getId());
		this.setName(country.getName());
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	
	public Country convertFromDAO(){
		Country country = new Country();
		country.setId(this.getId());
		country.setName(this.getName());
		return country;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(name);
		sb.append("]");
		return sb.toString();
	}
	
	
}
