package hr.fer.diplomski.taxi.service.model;


public class Address {

	private Long id;

	private long latitude;
	
	private long longitude;

	private City city;

	private String street;
	
	private String houseNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public long getLongitude() {
		return longitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(latitude);
		sb.append(", ");
		sb.append(longitude);
		sb.append(", ");
		sb.append(street);
		sb.append(", ");
		sb.append(houseNumber);
		sb.append(", ");
		sb.append(city);
		sb.append("]");
		return sb.toString();
	}
	
	
}
