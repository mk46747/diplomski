package hr.fer.diplomski.taxi.service.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class FarePriceList implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;

	private FarePriceListType farePriceListType;

	private BigDecimal baseFarePrice;
	
	private BigDecimal timeFarePricePerMin;
	
	private BigDecimal distanceFarePricePerKm;
	
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FarePriceListType getFarePriceListType() {
		return farePriceListType;
	}

	public void setFarePriceListType(FarePriceListType farePriceListType) {
		this.farePriceListType = farePriceListType;
	}

	public BigDecimal getBaseFarePrice() {
		return baseFarePrice;
	}

	public void setBaseFarePrice(BigDecimal baseFarePrice) {
		this.baseFarePrice = baseFarePrice;
	}

	public BigDecimal getTimeFarePricePerMin() {
		return timeFarePricePerMin;
	}

	public void setTimeFarePricePerMin(BigDecimal timeFarePricePerMin) {
		this.timeFarePricePerMin = timeFarePricePerMin;
	}

	public BigDecimal getDistanceFarePricePerKm() {
		return distanceFarePricePerKm;
	}

	public void setDistanceFarePricePerKm(BigDecimal distanceFarePricePerKm) {
		this.distanceFarePricePerKm = distanceFarePricePerKm;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(baseFarePrice);
		sb.append(", ");
		sb.append(timeFarePricePerMin);
		sb.append(", ");		
		sb.append(distanceFarePricePerKm);
		sb.append(", ");		
		sb.append(active);
		sb.append("]");
		return sb.toString();

	}
}
