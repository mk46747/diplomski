package hr.fer.diplomski.taxi.service.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.joda.time.DateTime;

public class Trip implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long id;

	private User user;

	private Taxi taxi;

	private Address start;

	private Address destination;
	
	private long distance;

	private DateTime startTime;

	private DateTime endTime;

	private FarePriceList farePriceList;
	
	private TripStatus tripStatus;
	
	private BigDecimal baseFare;
	
	private BigDecimal timeFare;
	
	private BigDecimal distanceFare;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Taxi getTaxi() {
		return taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	public Address getStart() {
		return start;
	}

	public void setStart(Address start) {
		this.start = start;
	}

	public Address getDestination() {
		return destination;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}



	public BigDecimal getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(BigDecimal baseFare) {
		this.baseFare = baseFare;
	}

	public BigDecimal getTimeFare() {
		return timeFare;
	}

	public void setTimeFare(BigDecimal timeFare) {
		this.timeFare = timeFare;
	}

	public BigDecimal getDistanceFare() {
		return distanceFare;
	}

	public void setDistanceFare(BigDecimal distanceFare) {
		this.distanceFare = distanceFare;
	}

	public FarePriceList getFarePriceList() {
		return farePriceList;
	}

	public void setFarePriceList(FarePriceList farePriceList) {
		this.farePriceList = farePriceList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TripStatus getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(TripStatus tripStatus) {
		this.tripStatus = tripStatus;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", user: ");
		sb.append(user);
		sb.append(", taxi: ");
		sb.append(taxi);
		sb.append(", fpl: ");
		sb.append(farePriceList);		
		sb.append(", start: ");
		sb.append(start);
		sb.append(", dest: ");
		sb.append(destination);
		sb.append(", distance: ");
		sb.append(distance);
		sb.append(", startTime: ");
		sb.append(startTime);		
		sb.append(", endTime: ");
		sb.append(endTime);	
		sb.append(", status: ");
		sb.append(tripStatus);	
		sb.append("]");
		return sb.toString();
	}
	
	
}
