package hr.fer.diplomski.taxi.service.model;


public class User extends Person{
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	
	private String password;
	
    private Role role;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(username);
		sb.append("]");
		return sb.toString();

	}

	
}
