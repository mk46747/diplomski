package hr.fer.diplomski.taxi.service.model;

import java.io.Serializable;


public class City implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;
	
	private Country country;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(name);
		sb.append("]");
		return sb.toString();
	}
	
}
