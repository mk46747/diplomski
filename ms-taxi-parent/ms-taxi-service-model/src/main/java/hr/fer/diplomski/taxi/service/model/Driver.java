package hr.fer.diplomski.taxi.service.model;

public class Driver extends Person{

	private static final long serialVersionUID = 1L;
	
	private String licenceNumber;
	
	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(licenceNumber);
		sb.append("]");
		return sb.toString();

	}
}
