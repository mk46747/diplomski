package hr.fer.diplomski.monitoringadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer 
public class MonitoringAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringAdminApplication.class, args);
	}

}

