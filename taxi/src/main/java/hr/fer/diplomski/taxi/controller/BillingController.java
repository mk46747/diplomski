package hr.fer.diplomski.taxi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.service.ReceiptService;
import hr.fer.diplomski.taxi.service.TripService;

@RestController
@RequestMapping("/taxi/billing")
public class BillingController {
	private final Logger LOGGER = LoggerFactory.getLogger(BillingController.class);
	
	private final ReceiptService receiptService;
	
	private final TripService tripService;
	
	public BillingController(final ReceiptService receiptService, final TripService tripService){
		this.receiptService = receiptService;
		this.tripService = tripService;
	}
	
	@GetMapping(value = "/receipt/{tripId}",  produces = MediaType.APPLICATION_PDF_VALUE)
	ResponseEntity<byte[]> getReceipt(@PathVariable Long tripId){
		LOGGER.info("Request get receipt for trip with id:{}", tripId);
		
		Trip trip = tripService.fetchTrip(tripId);

		byte[] pdf = receiptService.generatePdfReceipt(trip);
		if(pdf == null){
			LOGGER.warn("generated pdf is null");
			return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
		}
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=receipt.pdf");
        
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return responseEntity;
	}
}
