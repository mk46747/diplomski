package hr.fer.diplomski.taxi.security;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	private final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

	@Autowired
	private JwtConfig jwtConfig;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
  	protected void configure(HttpSecurity http) throws Exception {
		 http
			.csrf().disable()
			    // make sure we use stateless session; session won't be used to store user's state.
		 	    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) 	
			.and()
			    // handle an authorized attempts 
			    .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)) 	
			.and()
			   // Add a filter to validate the tokens with every request
			.addFilterBefore(new JwtTokenAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
			// authorization requests config
			.authorizeRequests()
			
			   // allow all who are accessing "auth" service
			.antMatchers(HttpMethod.POST, jwtConfig.getUriPublic()).permitAll()  
			  // .antMatchers(HttpMethod.GET, jwtConfig.getUri()).permitAll()  
			   // Any other request must be authenticated
			.anyRequest().authenticated();
		 
		http.addFilterAfter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), jwtConfig), UsernamePasswordAuthenticationFilter.class);
				//.authorizeRequests();
			   // allow all who are accessing "auth" service
			   //.antMatchers(HttpMethod.POST, "/taxi/user/public/**").permitAll()  
			   //.antMatchers(HttpMethod.POST, "/user/public/**").permitAll() ;

			
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Bean
  	public JwtConfig jwtConfig() {
		
		LOGGER.info("Instatiating JWT config");
		JwtConfig jwtConfig = new JwtConfig();
		LOGGER.info("Instatiated JWT config: {}", jwtConfig);
    	return jwtConfig;
  	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}

}
