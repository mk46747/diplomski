package hr.fer.diplomski.taxi.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import hr.fer.diplomski.taxi.controller.model.RegistrationRequest;
import hr.fer.diplomski.taxi.model.Role;
import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.service.UserService;

@RestController
@RequestMapping("/taxi/user")
public class UserController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);	
	
	private final UserService userService;
	
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(16);

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@PostMapping("/public/register")
	public ResponseEntity<?> registerUser(@RequestBody RegistrationRequest registrationRequest){
		
		LOGGER.info("Request register user: {}", registrationRequest);
		
		if(userService.existsByUsername(registrationRequest.getUsername())){
			LOGGER.info("Username taken, returning bad request");
			return new ResponseEntity<>("Username already taken", HttpStatus.BAD_REQUEST);
		}
		LOGGER.info("Creating new user");
		User user = createUserFromRequest(registrationRequest);
		User result = userService.create(user);
		LOGGER.info("Created new user: {}", result);
		
	    return ResponseEntity.ok().body(result);

	}


	@GetMapping("/{id}")
	ResponseEntity<?> getUser(@PathVariable Long id){
		
		LOGGER.info("Request to find user with id: {}", id);
		User user = userService.findById(id);
		if(user != null){
			LOGGER.info("Found user with id: {} ", id);
			return ResponseEntity.ok().body(user);	
		}
		LOGGER.warn("Did not find user with id: {} ", id);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	
	@PutMapping("/{id}")
	ResponseEntity<User> updateUser(@PathVariable Long id, @Valid @RequestBody User user){
		LOGGER.info("Request to update user: {}", user);
		User result = userService.update(user);
		return ResponseEntity.ok().body(result);
	}
	
	@DeleteMapping("/{id}")
	ResponseEntity<User> deleteUser(@PathVariable Long id){
		LOGGER.info("Request to delete user with id: {}", id);
		userService.delete(id);
		return ResponseEntity.ok().build();
	}
	

	private User createUserFromRequest(RegistrationRequest registrationRequest) {
		User user = new User();
		user.setFirstName(registrationRequest.getFirstName());
		user.setLastName(registrationRequest.getLastName());
		user.setEmail(registrationRequest.getEmail());
		user.setPhoneNumber(registrationRequest.getPhoneNumber());
		user.setUsername(registrationRequest.getUsername());
		user.setPassword(encoder.encode(registrationRequest.getPassword()));
		user.setCreditCardNumber(registrationRequest.getCreditCardNumber());
		user.setRole(Role.ROLE_USER);
	return user;
	}

}
