package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.User;

public interface UserService {
	
	User findByUsername(String username);
	
	User findByUsernameAndPassword(String username, String password);
	
	User findById(Long id);
	
	User create(User user);
	
	User update(User user);
	
	void delete(Long id);
		
	boolean existsByUsername(String username);

	
}
