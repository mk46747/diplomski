package hr.fer.diplomski.taxi.util;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.City;
import hr.fer.diplomski.taxi.model.Country;

public class AddressUtil {
	
	public static Address generateAddress(){
		Address address = new Address();
		Country country = new Country();
		country.setName("Hrvatska");
		City city = new City();
		city.setName("Zagreb");
		city.setCountry(country);
		address.setCity(city);
		address.setHouseNumber("3");
		address.setLatitude(Long.valueOf(123));
		address.setLongitude(Long.valueOf(321));
		address.setStreet("Unska");
		
		return address;
	}
	

}
