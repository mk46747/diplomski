package hr.fer.diplomski.taxi.service.impl;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.FarePriceList;
import hr.fer.diplomski.taxi.repository.FarePriceListRepository;
import hr.fer.diplomski.taxi.service.FarePriceListService;


@Service
public class FarePriceListServiceImpl implements FarePriceListService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(FarePriceListServiceImpl.class);

	
	private final FarePriceListRepository farePriceListRepository;
	
	@Autowired
	public FarePriceListServiceImpl(final FarePriceListRepository farePriceListRepository){
		this.farePriceListRepository = farePriceListRepository;
	}

	@Override
	public FarePriceList getFarePriceList() {
		FarePriceList fpl = null;
		try{
			fpl = farePriceListRepository.findByActive(Boolean.TRUE);
		}catch(EntityNotFoundException e){
			LOGGER.info("Entity FarePriceList not found for active: {}", Boolean.TRUE);
			//TODO exception
			return null;
		}
		LOGGER.info("Fetched active fpl: {} from DB", fpl);

		return fpl;
	}

}
