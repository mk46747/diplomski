package hr.fer.diplomski.taxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.taxi.model.FarePriceList;

@Repository
public interface FarePriceListRepository extends JpaRepository<FarePriceList, Long> {
	
	FarePriceList findByActive(Boolean active);
}
