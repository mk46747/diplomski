package hr.fer.diplomski.taxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.taxi.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
		
	User findByUsername(String username);
	
	User findByUsernameAndPassword(String username, String password);
	
	boolean existsByUsername(String username);
			
}
