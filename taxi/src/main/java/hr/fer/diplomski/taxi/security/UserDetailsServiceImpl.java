package hr.fer.diplomski.taxi.security;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.service.UserService;

@Service   
public class UserDetailsServiceImpl implements UserDetailsService  {
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	
	private UserService userService;
	
	public UserDetailsServiceImpl(final UserService userService) {
		this.userService = userService;
	}

	@Override
	public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		LOGGER.info("Invoked UserDetails loadUserByUsername, username: {}", username);
		
		User user = userService.findByUsername(username);
		LOGGER.info("userdetails - found user: {}", user);

		if(user != null) {
			
			List<GrantedAuthority> grantedAuthorities = AuthorityUtils
	                	.commaSeparatedStringToAuthorityList(user.getRole().getCode());
			LOGGER.info("userdetails - grantedAuthorities: {}", grantedAuthorities);

			return new CustomUserDetails(user.getPersonId(), user.getUsername(), user.getPassword(), grantedAuthorities);
		}
		
		
		throw new UsernameNotFoundException("Username: " + username + " not found");
	}
	
	
}