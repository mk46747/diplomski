package hr.fer.diplomski.taxi;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import hr.fer.diplomski.taxi.model.Driver;
import hr.fer.diplomski.taxi.model.FarePriceList;
import hr.fer.diplomski.taxi.model.Taxi;
import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.repository.FarePriceListRepository;
import hr.fer.diplomski.taxi.repository.TaxiRepository;
import hr.fer.diplomski.taxi.repository.UserRepository;
import hr.fer.diplomski.taxi.util.DriverUtil;
import hr.fer.diplomski.taxi.util.FplUtil;
import hr.fer.diplomski.taxi.util.TaxiUtil;
import hr.fer.diplomski.taxi.util.UserUtil;

@Component
public class Initializer implements CommandLineRunner {
	
	private final Logger LOGGER = LoggerFactory.getLogger(Initializer.class);
	
	private static final int TAXI_NUMBER = 12000;
	private static final int USER_NUMBER = 10000;
		
	private final TaxiRepository taxiRepository;
	
	private final FarePriceListRepository farePriceListRepository;
	
	
	private final UserRepository userRepository;
		
	@Autowired
	public Initializer(final TaxiRepository taxiRepository, final FarePriceListRepository farePriceListRepository,UserRepository userRepository) {
		this.taxiRepository = taxiRepository;
		this.farePriceListRepository = farePriceListRepository;
		this.userRepository = userRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		
		LOGGER.info("Generating {} Taxi objects...", TAXI_NUMBER);
		List<Taxi> taxiList = new ArrayList<Taxi>();
		for(int i = 0; i < TAXI_NUMBER; i++){
			Driver driver = DriverUtil.generateDriver(i);
			Taxi taxi = TaxiUtil.generateTaxi(i, driver);
			taxiList.add(taxi);
		}
		LOGGER.info("Saving Taxi objects.");
		taxiRepository.saveAll(taxiList);		
		LOGGER.info("Taxi objects saved.");

		LOGGER.info("Generating FarePriceList object...");
		FarePriceList fpl = FplUtil.generateFarePriceList();
		farePriceListRepository.save(fpl);
		LOGGER.info("FarePriceList object saved.");
		
//		LOGGER.info("Generating {} User objects...", USER_NUMBER);
//		List<User> userList = new ArrayList<User>();
//		for(int i = 0; i < USER_NUMBER; i++){
//			User user = UserUtil.generateUser(i);
//			userList.add(user);
//			//LOGGER.info("{}", i);
//
//		}
//		LOGGER.info("Saving User objects.");
//		userRepository.saveAll(userList);		
//		LOGGER.info("User objects saved.");

	}

}
