package hr.fer.diplomski.taxi.util;

import hr.fer.diplomski.taxi.model.Driver;

public class DriverUtil {
	
	public static final String DRIVER_FIRST_NAME_PREFIX = "driver_first_name_";
	public static final String DRIVER_LAST_NAME_PREFIX = "driver_last_name_";
	public static final String DRIVER_EMAIL_PREFIX = "driver_email_";
	public static final String DRIVER_PHONE_NUMBER_PREFIX = "driver_phone_number_";
	public static final String DRIVER_LICENCE_NUMBER_PREFIX = "driver_licence_number_";

	public static Driver generateDriver(int index){
		Driver driver = new Driver();
		driver.setFirstName(DRIVER_FIRST_NAME_PREFIX + index);
		driver.setLastName(DRIVER_LAST_NAME_PREFIX + index);
		driver.setEmail(DRIVER_EMAIL_PREFIX + index);
		driver.setPhoneNumber(DRIVER_PHONE_NUMBER_PREFIX + index);
		driver.setLicenceNumber(DRIVER_LICENCE_NUMBER_PREFIX + index);
		return driver;
	}
}
