package hr.fer.diplomski.taxi.model.billing;


public class Receipt{
	
	private String tripId;
	
	private String userFirstName;
	
	private String userLastName;
	
	private String userEmail;

	private String userCreditCard;
	
	
	private String driverFirstName;

	private String driverLastName;

	private String driverLicenceNumber;
	

	private String taxiRegistrationNumber;

	private String taxiVehicleModel;
	

	private String tripStartAddress;

	private String tripDestinationAddress;
	

	private String tripStartTime;

	private String tripEndTime;
	

	private String tripDistance;

	private String tripDuration;
	

	private String tripBaseFare;

	private String tripTimeFare;

	private String tripDistanceFare;

	private String tripTotalFare;
	

	
	public String getTripId() {
		return tripId;
	}



	public void setTripId(String tripId) {
		this.tripId = tripId;
	}



	public String getUserFirstName() {
		return userFirstName;
	}



	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}



	public String getUserLastName() {
		return userLastName;
	}



	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}



	public String getUserEmail() {
		return userEmail;
	}



	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}



	public String getUserCreditCard() {
		return userCreditCard;
	}



	public void setUserCreditCard(String userCreditCard) {
		this.userCreditCard = userCreditCard;
	}



	public String getDriverFirstName() {
		return driverFirstName;
	}



	public void setDriverFirstName(String driverFirstName) {
		this.driverFirstName = driverFirstName;
	}



	public String getDriverLastName() {
		return driverLastName;
	}



	public void setDriverLastName(String driverLastName) {
		this.driverLastName = driverLastName;
	}



	public String getDriverLicenceNumber() {
		return driverLicenceNumber;
	}



	public void setDriverLicenceNumber(String driverLicenceNumber) {
		this.driverLicenceNumber = driverLicenceNumber;
	}



	public String getTaxiRegistrationNumber() {
		return taxiRegistrationNumber;
	}



	public void setTaxiRegistrationNumber(String taxiRegistrationNumber) {
		this.taxiRegistrationNumber = taxiRegistrationNumber;
	}



	public String getTaxiVehicleModel() {
		return taxiVehicleModel;
	}



	public void setTaxiVehicleModel(String taxiVehicleModel) {
		this.taxiVehicleModel = taxiVehicleModel;
	}



	public String getTripStartAddress() {
		return tripStartAddress;
	}



	public void setTripStartAddress(String tripStartAddress) {
		this.tripStartAddress = tripStartAddress;
	}



	public String getTripDestinationAddress() {
		return tripDestinationAddress;
	}



	public void setTripDestinationAddress(String tripDestinationAddress) {
		this.tripDestinationAddress = tripDestinationAddress;
	}



	public String getTripStartTime() {
		return tripStartTime;
	}



	public void setTripStartTime(String tripStartTime) {
		this.tripStartTime = tripStartTime;
	}



	public String getTripEndTime() {
		return tripEndTime;
	}



	public void setTripEndTime(String tripEndTime) {
		this.tripEndTime = tripEndTime;
	}



	public String getTripDistance() {
		return tripDistance;
	}



	public void setTripDistance(String tripDistance) {
		this.tripDistance = tripDistance;
	}



	public String getTripDuration() {
		return tripDuration;
	}



	public void setTripDuration(String tripDuration) {
		this.tripDuration = tripDuration;
	}



	public String getTripBaseFare() {
		return tripBaseFare;
	}



	public void setTripBaseFare(String tripBaseFare) {
		this.tripBaseFare = tripBaseFare;
	}



	public String getTripTimeFare() {
		return tripTimeFare;
	}



	public void setTripTimeFare(String tripTimeFare) {
		this.tripTimeFare = tripTimeFare;
	}



	public String getTripDistanceFare() {
		return tripDistanceFare;
	}



	public void setTripDistanceFare(String tripDistanceFare) {
		this.tripDistanceFare = tripDistanceFare;
	}



	public String getTripTotalFare() {
		return tripTotalFare;
	}



	public void setTripTotalFare(String tripTotalFare) {
		this.tripTotalFare = tripTotalFare;
	}



	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[User firstName: ");
		sb.append(userFirstName);
		sb.append(", total fare: ");
		sb.append(tripTotalFare);
		sb.append(" ... ]");
		return sb.toString();
	}
	
}
