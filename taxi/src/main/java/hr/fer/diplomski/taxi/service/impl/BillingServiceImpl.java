package hr.fer.diplomski.taxi.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.billing.Receipt;
import hr.fer.diplomski.taxi.service.BillingService;
import hr.fer.diplomski.taxi.service.ReceiptService;

@Service
public class BillingServiceImpl implements BillingService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(BillingServiceImpl.class);
	
	private final ReceiptService receiptService;
	
	@Autowired
	public BillingServiceImpl(final ReceiptService receiptService){
		this.receiptService = receiptService;
	}
	
	@Override
	public void processTrip(Trip trip) {
		Receipt receipt = receiptService.composeReceipt(trip);
		if(receipt == null){
			LOGGER.error("Composed receipt is null!");
			return;
		}
		LOGGER.info("Composed receipt: {}", receipt);
		boolean success = chargeTrip(receipt);
		if(success == false){
			LOGGER.error("Charging failed...");
		}
		
		
		boolean emailSuccess = sendReceiptToEmail(receipt);
		if(emailSuccess == false){
			LOGGER.error("Sending to email failed...");
		}
	}
	
	private boolean sendReceiptToEmail(Receipt receipt) {
		LOGGER.info("Invoked send receipt to email");
		byte[] receiptPdf = receiptService.generatePdfReceipt(receipt);
		LOGGER.info("Generated pdf receipt, size: {} bytes", receiptPdf == null ? 0 : receiptPdf.length);
		String email = receipt.getUserEmail();
		LOGGER.info("Sending pdf receipt to: {}", email);
		LOGGER.info("...");
		LOGGER.info("...");
		LOGGER.info("Pdf receipt sent");
		return Boolean.TRUE;

	}
	
	private boolean chargeTrip(Receipt receipt) {
		LOGGER.info("Charging trip - creditCard: {} , total fare: {}, user first name: {}", receipt.getUserCreditCard(), receipt.getTripTotalFare(), receipt.getUserFirstName());
		LOGGER.info("...");
		LOGGER.info("...");
		LOGGER.info("Charged!");
		return Boolean.TRUE;
	}
	
	

}
