package hr.fer.diplomski.taxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.diplomski.taxi.model.Taxi;
import hr.fer.diplomski.taxi.model.TaxiStatus;

@Repository
public interface TaxiRepository extends JpaRepository<Taxi, Long>{

	Taxi findFirstByTaxiStatus(TaxiStatus taxiStatus);
}
