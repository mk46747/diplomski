package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.Taxi;

public interface TaxiService {

	Taxi findTaxi(Address start);
	
	Taxi updateTaxi(Taxi taxi);
}
