package hr.fer.diplomski.taxi.controller;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.service.TripService;
import hr.fer.diplomski.taxi.util.AddressUtil;

@RestController
@RequestMapping("/taxi/trip")
public class TripController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(TripController.class);
	
	private final TripService tripService;
		
	public TripController(final TripService tripService){
		this.tripService = tripService;
	}
	
	@GetMapping("")
	ResponseEntity<?> homePage(){
		return ResponseEntity.ok().body("");	

	}
	
	@PostMapping("/create/{userId}")
	ResponseEntity<Trip> createTrip(@PathVariable Long userId){
		LOGGER.info("Request create trip");
		//TODO
		Trip trip = tripService.createTrip(userId, AddressUtil.generateAddress(), AddressUtil.generateAddress());
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(trip);
	}
	
	@PostMapping("/start/{tripId}")
	ResponseEntity<Trip> startTrip(@PathVariable Long tripId){
		LOGGER.info("Request start trip");
		//TODO
		Trip trip = tripService.startTrip(tripId);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(trip);
	}
	
	@PostMapping("/finish/{tripId}")
	ResponseEntity<Trip> finishTrip(@PathVariable Long tripId){
		LOGGER.info("Request finish trip");
		//TODO
		Trip trip = tripService.finishTrip(tripId);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(trip);
	}
	
	@GetMapping("/user/{userId}")
	Collection<Trip> getUserTrips(@PathVariable Long userId){
		LOGGER.info("Request get trips for user with id: {}", userId);
		return tripService.getTripsForUser(userId);

	}
	

	

}
