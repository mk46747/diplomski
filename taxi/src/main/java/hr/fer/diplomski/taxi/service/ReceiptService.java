package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.billing.Receipt;

public interface ReceiptService {
	
	byte[] generatePdfReceipt(Trip trip);

	byte[] generatePdfReceipt(Receipt receipt);
	
	public Receipt composeReceipt(Trip trip);

}
