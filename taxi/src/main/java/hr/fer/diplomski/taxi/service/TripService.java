package hr.fer.diplomski.taxi.service;

import java.util.List;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.Trip;

public interface TripService {

	Trip createTrip(Long userId, Address start, Address destination);
	
	Trip startTrip(Long tripId);
	
	Trip finishTrip(Long tripId);

	List<Trip> getTripsForUser(Long userId);
	
	Trip fetchTrip(Long tripId);
}
