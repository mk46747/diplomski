package hr.fer.diplomski.taxi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;



@Entity
@Table(name = "tx_address")
public class Address implements Serializable{

	private static final long serialVersionUID = -4381720514847157786L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_address")
	@SequenceGenerator(name = "seq_tx_address", sequenceName = "seq_tx_address", allocationSize = 1)
	@Column(name = "address_id", nullable = false)
	private Long id;

	@Column(name = "address_latitude", nullable = false)
	private long latitude;
	
	@Column(name = "address_longitude", nullable = false)
	private long longitude;
	
	@ManyToOne( fetch = FetchType.EAGER)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "city_id")
	private City city;
	
	@Column(name = "address_street")
	private String street;
	
	@Column(name = "address_house_num")
	private String houseNumber;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public long getLongitude() {
		return longitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}


	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append(", ");
		sb.append(latitude);
		sb.append(", ");
		sb.append(longitude);
		sb.append(", ");
		sb.append(street);
		sb.append(", ");
		sb.append(houseNumber);
		sb.append(", ");
		sb.append(city);
		sb.append("]");
		return sb.toString();
	}	
	
	
}
