package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.billing.Receipt;

public interface JasperReportService {

	byte[] generateReceiptPdf(Receipt receipt);
}
