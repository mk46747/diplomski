package hr.fer.diplomski.taxi.security;

import org.springframework.beans.factory.annotation.Value;

public class JwtConfig {
	
    @Value("${security.jwt.uri.public}")
    private String uriPublic;
    
    @Value("${security.jwt.uri.login}")
    private String uriLogin;

    @Value("${security.jwt.header}")
    private String header;

    @Value("${security.jwt.prefix}")
    private String prefix;

    @Value("${security.jwt.expiration}")
    private int expiration;

    @Value("${security.jwt.secret}")
    private String secret;



	public String getUriPublic() {
		return uriPublic;
	}

	public void setUriPublic(String uriPublic) {
		this.uriPublic = uriPublic;
	}

	public String getUriLogin() {
		return uriLogin;
	}

	public void setUriLogin(String uriLogin) {
		this.uriLogin = uriLogin;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public int getExpiration() {
		return expiration;
	}

	public void setExpiration(int expiration) {
		this.expiration = expiration;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
    
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("Uri public: ");
		sb.append(uriPublic);
		sb.append(" Uri login: ");
		sb.append(uriLogin);
		sb.append(" header: ");
		sb.append(header);
		sb.append(" prefix: ");
		sb.append(prefix);
		sb.append(" expiration: ");
		sb.append(expiration);
		sb.append(" secret: ");
		sb.append(secret);
		return sb.toString();
	}
    
}
