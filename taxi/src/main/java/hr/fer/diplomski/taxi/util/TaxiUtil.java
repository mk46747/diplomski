package hr.fer.diplomski.taxi.util;

import hr.fer.diplomski.taxi.model.Driver;
import hr.fer.diplomski.taxi.model.Taxi;
import hr.fer.diplomski.taxi.model.TaxiStatus;

public class TaxiUtil {

	public static final String TAXI_REGISTRATION_NUMBER_PREFIX = "taxi_reg_num_";
	public static final String DRIVER_VEHICLE_MODEL_PREFIX = "taxi_vehicle_model_";

	public static Taxi generateTaxi(int index, Driver driver){
		Taxi taxi = new Taxi();
		taxi.setDriver(driver);
		taxi.setRegistrationNumber(TAXI_REGISTRATION_NUMBER_PREFIX + index);
		taxi.setVehicleModel(DRIVER_VEHICLE_MODEL_PREFIX + index);
		taxi.setTaxiStatus(TaxiStatus.FREE);
		return taxi;
	}
}
