package hr.fer.diplomski.taxi.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@DiscriminatorValue("person_user")  
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User extends Person{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "user_username")
	private String username;
	
	@Column(name = "user_password")
	private String password;
	
	@Column(name = "user_credit_card_num")
	private String creditCardNumber;
	
	@Column(name = "user_role")
	@Enumerated(EnumType.STRING)
    private Role role;
	
	public User(){
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(personId);
		sb.append(", ");
		sb.append(firstName);
		sb.append(", ");		
		sb.append(lastName);
		sb.append(", ");		
		sb.append(username);
		sb.append("]");
		return sb.toString();

	}
	
	

	
}
