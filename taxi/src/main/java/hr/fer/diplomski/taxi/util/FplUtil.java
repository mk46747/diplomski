package hr.fer.diplomski.taxi.util;

import java.math.BigDecimal;

import hr.fer.diplomski.taxi.model.FarePriceList;
import hr.fer.diplomski.taxi.model.FarePriceListType;



public class FplUtil {

	public static FarePriceList generateFarePriceList(){
		FarePriceList fpl = new FarePriceList();
		
		fpl.setActive(Boolean.TRUE);
		fpl.setBaseFarePrice(new BigDecimal(15));
		fpl.setDistanceFarePricePerKm(new BigDecimal(5.5));
		fpl.setTimeFarePricePerMin(new BigDecimal(2.75));
		fpl.setFarePriceListType(FarePriceListType.NORMAL);
		
		return fpl;
	}
}
