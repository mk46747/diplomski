package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.FarePriceList;

public interface FarePriceListService {
	
	FarePriceList getFarePriceList();

}
