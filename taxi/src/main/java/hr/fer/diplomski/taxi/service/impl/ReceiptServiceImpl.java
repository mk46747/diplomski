package hr.fer.diplomski.taxi.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.City;
import hr.fer.diplomski.taxi.model.Country;
import hr.fer.diplomski.taxi.model.Driver;
import hr.fer.diplomski.taxi.model.Taxi;
import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.model.billing.Receipt;
import hr.fer.diplomski.taxi.service.JasperReportService;
import hr.fer.diplomski.taxi.service.ReceiptService;

@Service
public class ReceiptServiceImpl implements ReceiptService{
	
	private final Logger LOGGER = LoggerFactory.getLogger(ReceiptServiceImpl.class);

	private final JasperReportService jasperReportService;
	

		
	@Autowired
	public ReceiptServiceImpl(final JasperReportService jasperReportService) {
		this.jasperReportService = jasperReportService;
	}


	@Override
	public byte[] generatePdfReceipt(Trip trip) {

		
		Receipt receipt = composeReceipt(trip);
		
		return generatePdfReceipt(receipt);
	}

	@Override
	public byte[] generatePdfReceipt(Receipt receipt) {
		LOGGER.info("Invoking jasper generate receipt for receipt: {}", receipt);
		byte[] receiptPdf = jasperReportService.generateReceiptPdf(receipt);
		LOGGER.info("Generated pdf receipt, size: {} bytes", receiptPdf == null ? 0 : receiptPdf.length);
		return receiptPdf;

	}
	
	@Override
	public Receipt composeReceipt(Trip trip){
		if(trip == null){
			LOGGER.error("Compose receipt - trip is null!");
			return null;
		}
		User user = trip.getUser();
		if(user == null){
			LOGGER.error("Compose receipt - user is null - Trip: {}", trip);
			return null;			
		}
		
		Taxi taxi = trip.getTaxi();
		if(taxi == null){
			LOGGER.error("Compose receipt - taxi is null! - Trip: {}", trip);
			return null;			
		}
		
		Driver driver = taxi.getDriver();
		if(driver == null){
			LOGGER.error("Compose receipt - driver is null - Taxi: {}!", taxi);
			return null;			
		}
		
		Receipt receipt = new Receipt();
		receipt.setTripId(trip.getTripId().toString());
		
		receipt.setUserFirstName(user.getFirstName());
		receipt.setUserLastName(user.getLastName());
		receipt.setUserEmail(user.getEmail());
		receipt.setUserCreditCard(user.getCreditCardNumber());
		
		receipt.setDriverFirstName(driver.getFirstName());
		receipt.setDriverLastName(driver.getLastName());
		receipt.setDriverLicenceNumber(driver.getLicenceNumber());
		
		receipt.setTaxiVehicleModel(taxi.getVehicleModel());
		receipt.setTaxiRegistrationNumber(taxi.getRegistrationNumber());
		
		receipt.setTripStartTime(convertLocalDateTimeToString(trip.getStartTime()));
		receipt.setTripEndTime(convertLocalDateTimeToString(trip.getEndTime()));
		
		receipt.setTripStartAddress(convertAddressToString(trip.getStart()));
		receipt.setTripDestinationAddress(convertAddressToString(trip.getDestination()));
		
		receipt.setTripDistance(convertDistanceToString(trip.getDistance()));
		receipt.setTripDuration(convertDurationToString(trip));
		
		receipt.setTripBaseFare(convertFareToString(trip.getBaseFare()));
		receipt.setTripTimeFare(convertFareToString(trip.getTimeFare()));
		receipt.setTripDistanceFare(convertFareToString(trip.getDistanceFare()));
		receipt.setTripTotalFare(calculateTotalFare(trip));
		
		return receipt;
	}
	
	private String calculateTotalFare(Trip trip) {
		BigDecimal totalFare = new BigDecimal(0);
		BigDecimal baseFare = trip.getBaseFare();
		BigDecimal distanceFare = trip.getDistanceFare();
		BigDecimal timeFare = trip.getTimeFare();
		
		totalFare = totalFare.add(baseFare == null ? new BigDecimal(0) : baseFare);
		totalFare = totalFare.add(distanceFare == null ? new BigDecimal(0) : distanceFare);
		totalFare = totalFare.add(timeFare == null ? new BigDecimal(0) : timeFare);
		
		return convertFareToString(totalFare);
	}
	
	private String convertFareToString(BigDecimal value) {
		DecimalFormat df = new DecimalFormat("#,###.00");
	    return df.format(value) + " kn";
	}
	
	private String convertDurationToString(Trip trip){
		LocalDateTime start = trip.getStartTime();
		LocalDateTime end = trip.getEndTime();
		if(start == null || end == null){
			return "";
		}
		Period duration = new Period(start, end);
		
		PeriodFormatter hoursMinutes = new PeriodFormatterBuilder()
				.appendHours()
			    .appendSeparator(" hour", " hours")
			    .appendMinutes()
			    .appendSuffix(" minute", " minutes")
			    .appendSeconds()
			    .appendSuffix(" second", " seconds")
			    .toFormatter();
		return hoursMinutes.print(duration);
	}
	
	private String convertDistanceToString(Long distance){
		return distance + " km";
	}
	
	private String convertLocalDateTimeToString(LocalDateTime dateTime){
		return dateTime == null ? "" : dateTime.toString(DateTimeFormat.fullDateTime());
	}
	
	private String convertAddressToString(Address address){
		if(address == null){
			return null;
		}
		String cityName = "";
		String countryName = "";
		
		City city = address.getCity();
		if(city != null){
			cityName = city.getName();
			Country country = city.getCountry();
			if(country != null){
				countryName = country.getName();
			}
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(address.getStreet());
		sb.append(" ");
		sb.append(address.getHouseNumber());
		sb.append(", ");
		sb.append(cityName);
		sb.append(", ");
		sb.append(countryName);
		return sb.toString();

	}


}
